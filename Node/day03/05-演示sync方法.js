// // 使用读文件的方法 来演示同步(Synchronize)和异步(asynchronous)
// const fs = require("fs");
// const path = require('path');
// // 先使用异步
// // console.log("我是111");
// // fs.readFile(path.join(__dirname, "./files/1.txt"), 'utf8', (error, data) => {
// //   if (error) {
// //     console.log(error);
// //   }
// //   console.log(data);
// // })

// // // 先使用异步
// // console.log("我是222");
// // fs.readFile(path.join(__dirname, "./files/2.txt"), 'utf8', (error, data) => {
// //   if (error) {
// //     console.log(error);
// //   }
// //   console.log(data);
// // })
// // console.log("我是333");
// // 同步: 有顺序关系的  而且有先后

// console.log("1");
// var result1 = fs.readFileSync(path.join(__dirname, "./files/1.txt"), 'utf8');
// console.log(result1);
// console.log("2");
// var result2 = fs.readFileSync(path.join(__dirname, "./files/2.txt"), 'utf8');
// console.log(result2);
// console.log("3");













// 使用读文件的方法 来演示同步(Synchronize)和异步(asynchronous)
// const fs = require("fs");
// const path = require('path');
// // 先使用异步
// // console.log("我是111");
// // fs.readFile(path.join(__dirname, "./files/1.txt"), 'utf8', (error, data) => {
// //   if (error) {
// //     console.log(error);
// //   }
// //   console.log(data);
// // })

// // // 先使用异步
// // console.log("我是222");
// // fs.readFile(path.join(__dirname, "./files/2.txt"), 'utf8', (error, data) => {
// //   if (error) {
// //     console.log(error);
// //   }
// //   console.log(data);
// // })
// // console.log("我是333");
// // 同步: 有顺序关系的  而且有先后

// console.log("1");
// var result1 = fs.readFileSync(path.join(__dirname, "./files/1.txt"), 'utf8');
// console.log(result1);
// console.log("2");
// var result2 = fs.readFileSync(path.join(__dirname, "./files/2.txt"), 'utf8');
// console.log(result2);
// console.log("3");






// 使用读文件的方法 来演示同步(Synchronize)和异步(asynchronous)
const fs = require("fs");
const path = require('path');
// 先使用异步
// console.log("我是111");
// fs.readFile(path.join(__dirname, "./files/1.txt"), 'utf8', (error, data) => {
//   if (error) {
//     console.log(error);
//   }
//   console.log(data);
// })

// // 先使用异步
// console.log("我是222");
// fs.readFile(path.join(__dirname, "./files/2.txt"), 'utf8', (error, data) => {
//   if (error) {
//     console.log(error);
//   }
//   console.log(data);
// })
// console.log("我是333");
// 同步: 有顺序关系的  而且有先后

console.log("1");
var result1 = fs.readFileSync(path.join(__dirname, "./files/1.txt"), 'utf8');
console.log(result1);
console.log("2");
var result2 = fs.readFileSync(path.join(__dirname, "./files/2.txt"), 'utf8');
console.log(result2);
console.log("3");