// const fs = require('fs')
// const path = require('path')

// console.log(path.join(__dirname, './files/1.txt'))

// // 记住：今后只要涉及到路径片段的拼接，一定要使用 path.join() 方法
// fs.readFile(path.join(__dirname, './files/1.txt'), 'utf-8', (err, dataStr) => {
//   if (err) return console.log(err.message)
//   console.log(dataStr)
// })





// // 引包
// const fs = require('fs')
// const path = require('path')
// // 用包
// console.log(path.join(__dirname, './files/1.txt'))

// // 记住：今后只要涉及到路径片段的拼接，一定要使用 path.join() 方法
// fs.readFile(path.join(__dirname, './files/1.txt'), 'utf-8', (err, dataStr) => {
//   if (err) return console.log(err.message)
//   console.log(dataStr);

// })







// 引包
const fs = require('fs')
const path = require('path')
// 用包
console.log(path.join(__dirname, './files/1.txt'))

// 记住：今后只要涉及到路径片段的拼接，一定要使用 path.join() 方法
fs.readFile(path.join(__dirname, './files/1.txt'), 'utf-8', (err, dataStr) => {
  if (err) return console.log(err.message)
  console.log(dataStr)
})