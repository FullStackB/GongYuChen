// CommonJs中规定了 一个js就是一个模块
// 模块中有exports输出 有require输入 
// 除了输出和输入 其中在js文件中的任何变量 方法不被外界所访问
// 任何一个变量和方法 在Node中的最大作用域就是模块
// Node中没有全局作用域 最大就是模块

// let a = 10;
// function fn() {
//   console.log(111);
// }
// let arr = [12, 3, 4, 5];
// let fn = function () {
//   console.log(111);
// }
// // 

// module.exports = {
//   a,
//   fn,
//   arr
// }

// module.exports = {
//   a: a,
//   fn: fn,
//   arr1: arr
// }



// 演示全局变量全局函数 但是明令禁止用全局
// 请问浏览器中的顶级对象是谁? window
// Node中的顶级对象是global(全局的意思)
// console.log(global);
// global.a = 10;
// global.show = function () {
//   console.log("我是全局的函数")
// }
// 因为现阶段我们写的代码都是js 模块又多  难免会有相同的变量
// 如果你写的是全局的 就会造成变量污染 程序会挂掉 还很难找








// CommonJs中规定了 一个js就是一个模块
// 模块中有exports输出 有require输入 
// 除了输出和输入 其中在js文件中的任何变量 方法不被外界所访问
// 任何一个变量和方法 在Node中的最大作用域就是模块
// Node中没有全局作用域 最大就是模块

// let a = 10;
// function fn() {
//   console.log(111);
// }
// let arr = [12, 3, 4, 5];
// let fn = function () {
//   console.log(111);
// }
// // 

// module.exports = {
//   a,
//   fn,
//   arr
// }

// module.exports = {
//   a: a,
//   fn: fn,
//   arr1: arr
// }

// 演示全局变量全局函数 但是明令禁止用全局
// 请问浏览器中的顶级对象是谁? window
// Node中的顶级对象是global(全局的意思)
// console.log(global);
// global.a = 10;
// global.show = function () {
  // console.log("我是全局的函数")
// }
// 因为现阶段我们写的代码都是js 模块又多  难免会有相同的变量
// 如果你写的是全局的 就会造成变量污染 程序会挂掉 还很难找










// CommonJs中规定了 一个js就是一个模块
// 模块中有exports输出 有require输入 
// 除了输出和输入 其中在js文件中的任何变量 方法不被外界所访问
// 任何一个变量和方法 在Node中的最大作用域就是模块
// Node中没有全局作用域 最大就是模块

// let a = 10;
// function fn() {
//   console.log(111);
// }
// let arr = [12, 3, 4, 5];
// let fn = function () {
//   console.log(111);
// }
// // 

// module.exports = {
//   a,
//   fn,
//   arr
// }

// module.exports = {
//   a: a,
//   fn: fn,
//   arr1: arr
// }

// 演示全局变量全局函数 但是明令禁止用全局
// 请问浏览器中的顶级对象是谁? window
// Node中的顶级对象是global(全局的意思)
// console.log(global);
global.a = 10;
global.show = function () {
  console.log("我是全局的函数")
}
// 因为现阶段我们写的代码都是js 模块又多  难免会有相同的变量
// 如果你写的是全局的 就会造成变量污染 程序会挂掉 还很难找