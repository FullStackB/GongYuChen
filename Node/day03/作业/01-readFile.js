//1、使用node.js对文件‘read.txt’中的内容“我是传智专修学院，全栈应用方向专业的一名大大一学生，我非常自豪”进行读取，并且在控制台输出
const fs = require('fs');

// 读取
fs.readFile('./01-read.txt', (error, data) => {
  // 判断
  if (error) {
    console.log(error.message);
  }
  // 输出
  console.log(data.toString());
})