// 求1!+2!+3!+...+10!的和
   
   // 计算一个数的阶乘
    var jc = function (num) {
      var result = 1;
      for (var i = 1; i <= num; i++) {
        result*=i;
      } 
      return result;
    }
    // 计算和
    var sum = function (num) {
      var he = 0;
      for (var i = 1; i <= num; i++) {
        he += jc(i);
      }
    
      return he;
    }
    
    console.log(sum(10));