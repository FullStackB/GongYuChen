// // fs模块 是用来操作文件的

// // node自带path
// const path = require('path');
// // 1.path.sep 返回当前系统的片段符
// // console.log(path.sep);
// // 2.path.dirname 返回某个文件所在的目录

// let str = "F:/FullStackB/Node/day03/code/03-path路径操作模块.js"
// // console.log(__dirname);
// // console.log(path.dirname(str));
// // 3.path.extname  extname扩展名后缀
// // console.log(path.extname(str));
// // 4.path.join(重要的) 拼接路径
// console.log(__dirname + './files/成绩.txt'); //F: \FullStackB\Node\day03\code / files / 成绩.txt
// //F:\FullStackB\Node\day03\code./files / 成绩.txt

// console.log(path.join(__dirname, './files/成绩.txt')); //F:\FullStackB\Node\day03\code\files\成绩.txt

// console.log(path.join('f:', "/a", "/b", "./c", "../d/aa.txt"));










// fs模块 是用来操作文件的

// node自带path
const path = require('path');
// 1.path.sep 返回当前系统的片段符
// console.log(path.sep);
// 2.path.dirname 返回某个文件所在的目录
let str = "G:/FullStackB/Node/day03/code/03-path路径操作模块.js"
// console.log(__dirname);
// console.log(path.dirname(str));
// 3.path.extname  extname扩展名后缀
// console.log(path.extname(str));
// 4.path.join(重要的) 拼接路径
console.log(__dirname + './files/成绩.txt'); //F: \FullStackB\Node\day03\code / files / 成绩.txt
// 出现错误的地址 F:\FullStackB\Node\day03\code./files / 成绩.txt
// 正确的路径
console.log(path.join(__dirname, './files/成绩.txt')); //F:\FullStackB\Node\day03\code\files\成绩.txt
// ../存在时 保留../并将前一个取代
console.log(path.join('f:', "/a", "/b", "..d/aa.txt"));
// f:\a\d\aa.txt
// ./ 存在时正常排序
console.log(path.join('f:', "/a", "/b", "./c", "/aa.txt"));
// f:\a\b\c\aa.txt








// node自带path
const path = require('path');
// 1.path.sep 返回当前系统的片段符
// console.log(path.sep);
// 2.path.dirname 返回某个文件所在的目录
let str = "G:/FullStackB/Node/day03/code/03-path路径操作模块.js"
// console.log(__dirname);
// console.log(path.dirname(str));
// 3.path.extname  extname扩展名后缀
// console.log(path.extname(str));
// 4.path.join(重要的) 拼接路径
console.log(__dirname + './files/成绩.txt'); //F: \FullStackB\Node\day03\code / files / 成绩.txt
// 出现错误的地址 F:\FullStackB\Node\day03\code./files / 成绩.txt
// 正确的路径
console.log(path.join(__dirname, './files/成绩.txt')); //F:\FullStackB\Node\day03\code\files\成绩.txt
// ../存在时 保留../并将前一个取代
console.log(path.join('f:', "/a", "/b", "..d/aa.txt"));
// f:\a\d\aa.txt
// ./ 存在时正常排序
console.log(path.join('f:', "/a", "/b", "./c", "/aa.txt"));
// f:\a\b\c\aa.txt