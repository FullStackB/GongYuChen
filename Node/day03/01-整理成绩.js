// 需求: 用Node读取 成绩.txt中的数据 然后把数据写入到 result.txt中
// 格式是这样的：
//  小红：99
//  小白：100
//  小黄：70
//  小黑：66
//  小绿：88


// 思路: 使用fs模块，通过readFile方法把成绩.txt中的数据读取出来 数据是个字符串
// 使用split方法把字符串分割成字符串数组 把‘=’号替换成‘:’  
// 把数组变成字符串 就使用数组的join方法在每个元素后面添加一个换行符 让其换行

// 步骤:
// // 1.引入fs模块
// const fs = require('fs');
// // 2.使用fs模块的readFile读取文件
// fs.readFile(__dirname + '/files/成绩.txt', 'utf8', (error, data) => {
//   if (error) {
//     console.log(error);
//   }
//   // 2.1 把读取出来的字符串数据变成字符串数组(split)
//   var scoreArr = data.split(' ');
//   // 2.2 遍历数组 把每一项中的 = 换成 :
//   var newArr = [];
//   scoreArr.forEach((item) => {
//     if (item.length > 0) {

//       var res = item.replace('=', ':');
//       newArr.push(res);
//     }
//   })
//   // 2.3 把数组转换成字符串 join方法(插入换行符)
//   var result = newArr.join('\n');
//   // 2.4 把处理完的数据写入result.txt中
//   fs.writeFile(__dirname + '/files/result.txt', result, 'utf8', (error) => {
//     if (error) {
//       console.log('文件写入失败');
//       return 0;
//     }
//     console.log('写入成功');
//   })
// })

























// // 1.引入fs模块
// const fs = require('fs');
// // 2.使用fs模块的readFile读取文件
// fs.readFile(__dirname + '/files/成绩.txt', 'utf8', (error, data) => {
//   if (error) {
//     console.log(error);
//   }
//   // 2.1 把读取出来的字符串数据变成字符串数组(split)
//   var scoreArr = data.split(' ');
//   // 2.2 遍历数组 把每一项中的 = 换成 :
//   var newArr = [];
//   scoreArr.forEach((item) => {
//     if (item.length > 0) {

//       var res = item.replace('=', ':');
//       newArr.push(res);
//     }
//   })
//   // 2.3 把数组转换成字符串 join方法(插入换行符)
//   var result = newArr.join('\n');
//   // 2.4 把处理完的数据写入result.txt中
//   fs.writeFile(__dirname + '/files/result.txt', result, 'utf8', (error) => {
//     if (error) {
//       console.log('文件写入失败');
//       return 0;
//     }
//     console.log('写入成功');
//   })
// })

























// 1.引入fs模块
const fs = require('fs');
// 2.使用fs模块的readFile读取文件
fs.readFile(__dirname + '/files/成绩.txt', 'utf8', (error, data) => {
  if (error) {
    console.log(error);
  }
  // 2.1 把读取出来的字符串数据变成字符串数组(split)
  var scoreArr = data.split(' ');// 中间必须有空格i
  // 2.2 遍历数组 把每一项中的 = 换成 :
  var newArr = [];
  scoreArr.forEach((item) => {
    if (item.length > 0) {

      var res = item.replace('=', ':');
      newArr.push(res);
    }
  })
  // 2.3 把数组转换成字符串 join方法(插入换行符)
  var result = newArr.join("\n");
  // 2.4 把处理完的数据写入result.txt中
  fs.writeFile(__dirname + '/files/result.txt', result, 'utf8', (error) => {
    if (error) {
      console.log('文件写入失败');
      return 0;
    }
    console.log('写入成功');
  })
})

