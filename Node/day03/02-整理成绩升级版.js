// 前置知识 filter
// let arr = [13, 0, 123, 56];
// var result = arr.filter(function (item, index) {
//   return item >= 13;
// })
// var result = arr.filter((item) => {
//   return item >13
// })
// console.log(result);

// 前置知识map
// let array1 = [1, 4, 6, 9, 10];
// var newArr1 = array1.map(function (item) {
//   return item + '*';
// })
// console.log(newArr1);

// 需求: 用Node读取 成绩.txt中的数据 然后把数据写入到 result.txt中
// 格式是这样的:
//  小红:99
//  小白:100
//  小黄:70
//  小黑:66
//  小绿:88

// 步骤:
// 1.引入fs模块
// const fs = require('fs');
// // 2.使用fs模块的readFile读取文件
// fs.readFile(__dirname + '/files/成绩.txt', 'utf8', (error, data) => {
//   if (error) {
//     console.log(error);
//   }
//   // 2.1 把读取出来的字符串数据变成字符串数组(split) 这一步的作用是什么
//   // 2.2 遍历数组 
//   var scoreArr = data.split(' ').filter((item) => { return item.length > 0 });
//   console.log(scoreArr);
//   // 把每一项中的 = 换成 :
//   var newArr = scoreArr.map((item) => { return item.replace('=', ':') })
//   console.log(newArr);
//   // 2.3 把数组转换成字符串 join方法(插入换行符)
//   // 2.4 把处理完的数据写道result.txt中
//   fs.writeFile(__dirname + '/files/result.txt', newArr.join('\n'), 'utf8', (error) => {
//     if (error) {
//       console.log("文件写入失败");
//       return false;
//     }
//     console.log('写入成功');
//   })

// })











// 1. 引入
// const fs = require('fs');
// // 2.使用fs模块的readFile读取文件
// fs.readFile(__dirname + '/files/成绩.txt', 'utf8', (error, data) => {
//   if (error) {
//     console.log(error);
//   }
//   // 2.1 把读取出来的字符串数据变成字符串数组(split) 这一步的作用是什么
//   // 2.2 遍历数组 
//   var scoreArr = data.split(' ').filter((item) => { return item.length > 0 });
//   console.log(scoreArr);
//   // 把每一项中的 = 换成 :
//   var newArr = scoreArr.map((item) => { return item.replace('=', ':') })
//   console.log(newArr);
//   // 2.3 把数组转换成字符串 join方法(插入换行符)
//   // 2.4 把处理完的数据写道result.txt中
//   fs.writeFile(__dirname + '/files/result.txt', newArr.join('\n'), 'utf8', (error) => {
//     if (error) {
//       console.log("文件写入失败");
//       return false;
//     }
//     console.log('写入成功');
//   })

// })












//  1. 引入
const fs = require('fs');
// 2.使用fs模块的readFile读取文件
fs.readFile(__dirname + '/files/成绩.txt', 'utf8', (error, data) => {
  if (error) {
    console.log(error);
  }
  // 2.1 把读取出来的字符串数据变成字符串数组(split) 这一步的作用是什么
  // 2.2 遍历数组 
  var scoreArr = data.split(' ').filter((item) => { return item.length > 0 });
  console.log(scoreArr);
  // 把每一项中的 = 换成 :
  var newArr = scoreArr.map((item) => { return item.replace('=', ':') })
  console.log(newArr);
  // 2.3 把数组转换成字符串 join方法(插入换行符)
  // 2.4 把处理完的数据写道result.txt中
  fs.writeFile(__dirname + '/files/result.txt', newArr.join('\n'), 'utf8', (error) => {
    if (error) {
      console.log("文件写入失败");
      return false;
    }
    console.log('写入成功');
  })

})