// 为什么要设置静态资源目录



// 设置静态资源目录

// 语法: app.use(express.static("真实存在的文件夹")) 当我们想要去获取静态资源目录中的文件的时候，就直接忽略了
// 变成 静态资源目录的文件夹   如果use后面没有路径标识符 那么就不需要写直接跳过 真实存在的文件夹
// 如果你所设置的那个静态资源目录中有index.html 会自动去找index.html


// 语法: app.use(路径标识符, express.static("真实存在的文件夹")) 如果你要访问 静态资源文件夹中的文件 必去要这样做
//  路径标识符+ 你要访问的文件的名字(该文件所在的目录)

// const express = require('express');
// const app = express();

// // app.use(express.static('public'));
// app.use('/public', express.static('./public'));

// app.listen(80, () => {
//   console.log('服务器运行中...1');
// })


const express = require('express');

const app = express()



