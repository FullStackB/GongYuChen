// const express = require('express');
// const bodyParser = require('body-parser');

// const app = express();
// // 使用urlencoded解析前端传来的数据  会把数据解析成字符串或数组
// app.use(bodyParser.urlencoded({ extended: false }))
// // 3.设置静态资源目录
// app.use(express.static('public'));

// // 4.当浏览器 使用post方式请求 /login的时候 返回一个响应
// app.post('/login', (req, res) => {
//   console.log(req.body);
//   res.send(req.body);
// })

// app.listen(80, () => {
//   console.log("服务器运行中...");
// })











// // 引入包
// const express = require('express');
// const bodyParser = require('body-parser');
// // 用express创建一个服务器
// const app = express();
// // 使用urlencoded解析前端传来的数据  会把数据解析成字符串或数组
// app.use(bodyParser.urlencoded({ extended: false }))
// //设置静态资源目录
// app.use(express.static('public'));

// // 当浏览器 使用post方式请求 /login的时候 返回一个响应
// app.post('/login', (req, res) => {
//   console.log(req.body);
//   res.send(req.body);
// })
// // 给服务器指定端口 并启动服务
// app.listen(80, () => {
//   console.log("服务器运行中...");
// })










// 引入包
const express = require('express');
const bodyParser = require('body-parser');
// 用express创建一个服务器
const app = express();
// 使用urlencoded解析前端传来的数据  会把数据解析成字符串或数组
app.use(bodyParser.urlencoded({ extended: false }))
// 设置静态资源目录
app.use(express.static('public'));
// 当浏览器 使用post方式请求 /login的时候 返回一个响应
app.post('/login', (req, res) => {
  console.log(req.body);
  res.send(req.body);
})
// 给服务器指定端口 并启动服务
app.listen(80, () => {
  console.log("服务器运行中...");
})