// // 1.引入express 包
// const express = require('express');

// // 2.使用expressc创建服务器
// const app = express();

// // 3.设置静态资源目录
// app.use(express.static('public'));

// // 4.当浏览器 请求/login的时候 服务器返回一下  浏览器在表单中输入的内容
// app.get('/login', (req, res) => {
//   res.send(req.query);
// })

// // 5.启动服务 指定端口
// app.listen(80, () => {
//   console.log('服务器运行中...1');
// })


// 127.0.0.1 本机回环地址    对应的网址 localhost
// 255.255.255.255  广播地址











// // 1.引入express 包
// const express = require('express');
// // 2.使用expressc创建服务器
// const app = express();
// // 3.设置静态资源目录
// app.use(express.static('public'));
// // 4.当浏览器 请求/login的时候 服务器返回一下  浏览器在表单中输入的内容
// app.get('/login', (req, res) => {
//   res.send(req.query);
// })
// // 5.启动服务 指定端口
// app.listen(80, () => {
//   console.log('服务器运行中...1');
// })








// 1.引入express 包
const express = require('express');
// 使用expressc创建服务器
const app = express();
// 设置静态资源目录
app.use(express.static('public'));
// 当浏览器 请求/login的时候 服务器返回一下  浏览器在表单中输入的内容
app.get('/login', (req, res) => {
  res.send(req.query);
})
// 指定服务器端口 启动服务
app.listen(80, () => {
  console.log('服务器运行中...1');
})