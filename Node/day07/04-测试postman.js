//1. 为什么要学习express框架
// 因为方便 express框架已经为我们开发了很多有用的包  便于我们开发

// 2.什么是express框架
// 在node的原生方法的基础之上做了一层封装   高度包容、快速而极简的 Node.js Web 框架

// 3.express 和node的关系
// 相当于js和jquery的关系


// 4.express如何用
// 4.1 下载包
// 4.2 引入包
// 4.3 用包 http://expressjs.com/zh-cn/starter/hello-world.html


// // 引入包
// const express = require('express');
// const bodyParser = require('body-parser');

// // 通过express 创建一个服务器
// const app = express();
// // 使用urlencoded解析前端传来的数据  会把数据解析成字符串或数组
// app.use(bodyParser.urlencoded({ extended: false }))
// app.get('/login', (req, res) => {
//   res.send(req.query);
// })

// app.post('/report', (req, res) => {
//   res.send(req.body);
// })

// // 给服务器指定端口 启动服务
// app.listen(8080, () => {
//   console.log("请访问： http://127.0.0.1");
// })







// 引入包
// const express = require('express');
// const bodyParser = require('body-parser');
// // 通过express创建一个服务器
// const app = express();
// // 使用urlencoded解析前端传来的数据  会把数据解析成字符串或数组
// app.use(bodyParser.urlencoded({ extended: false }))
// app.get('/login', (req, res) => {
//   res.send(req.query);
// })
// app.post('/report', (req, res) => {
//   res.send(req.body);
// })
// // 给服务器指定端口 启动服务
// app.listen(3000, () => {
//   console.log("请访问： http://127.0.0.1:3000");
// })










// 引入包
// const express = require('express');
// const bodyParser = require('body-parser');
// // 通过express创建一个服务器
// const app = express();
// // 使用urlencoded解析前端传来的数据  会把数据解析成字符串或数组
// app.use(bodyParser.urlencoded({ extended: false }))
// app.get('/login', (req, res) => {
//   res.send(req.query);
// })
// app.post('/report', (req, res) => {
//   res.send(req.body);
// })
// // 给服务器指定端口 启动服务
// app.listen(3000, () => {
//   console.log("请访问： http://127.0.0.1:3000");
// })