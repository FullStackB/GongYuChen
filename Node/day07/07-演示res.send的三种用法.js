// // 1.引入express 包
// const express = require('express');
// const fs = require('fs');
// const path = require('path');
// // 2.使用expressc创建服务器
// const app = express();

// // // 3.设置静态资源目录
// // app.use(express.static('public'));
// app.get('/', (req, res) => {
//   // send的三种用法
//   // 1.发送字符串给浏览器
//   // res.send("我是send发送的字符串");
//   // 2.发送对象或数组给浏览器
//   // res.send(['a', 'b', 'c']);
//   // res.send({ name: "jerry", age: 18 })
//   // 3.发送buffer 这种二进制数据给浏览器
//   fs.readFile(path.join(__dirname, "./music/毛不易 - 像我这样的人 (Live).flac"), (error, data) => {
//     if (error) {
//       console.log(error);
//     }

//     res.send(data);
//   })
// })

// // 5.启动服务 指定端口
// app.listen(80, () => {
//   console.log('服务器运行中...1');
// })


// // 127.0.0.1 本机回环地址    对应的网址 localhost
// // 255.255.255.255  广播地址



// // 1.引入express 包
// const express = require('express');
// const fs = require('fs');
// const path = require('path');
// // 2.使用expressc创建服务器
// const app = express();
// // // 3.设置静态资源目录
// // app.use(express.static('public'));
// app.get('/', (req, res) => {
//   // send的三种用法
//   // 1.发送字符串给浏览器
//   res.send("我是send发送的字符串");
//   // 2.发送对象或数组给浏览器
//   // res.send(['a', 'b', 'c']);
//   // res.send({ name: "jerry", age: 18 })
//   // 3.发送buffer 这种二进制数据给浏览器
//   // fs.readFile(path.join(__dirname, "./music/毛不易 - 像我这样的人 (Live).flac"), (error, data) => {
//   //   if (error) {
//   //     console.log(error);
//   //   }
//   //   res.send(data);
//   // })
// })
// // 5.启动服务 指定端口
// app.listen(80, () => {
//   console.log('服务器运行中...1');
// })




























// // 引包
// const express = require('express');
// const fs = require('fs');
// const path = require('path')
// // 创建一个服务器
// const app = express();
// // 设置静态资源 
// app.use(express.static('public'));
// // 根据url标识符的不同
// app.get('/', (req, res) => {

//   // res.send(['a', 'b', 'c']);
//   res.send({ name: 'gc', age: 19 })
//   // fs.readFile(path.join(__dirname, "./music/毛不易 - 像我这样的人 (Live).flac"), (err, data) => {
//   //   if (err) {
//   //     console.log(err);
//   //   }
//   //   res.send(data);
//   // })
// })

// app.listen(80, () => {
//   console.log('服务器正在运行...')
// })