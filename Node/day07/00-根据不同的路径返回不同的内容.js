// 原生Node根据不同路径返回不同内容

// const http = require('http');

// const server = http.createServer();

// server.on('request', (req, res) => {
//   res.writeHead(200, { 'Content-Type': 'text/html;charset=utf8' })
//   // 根据不同的路径标识符返回不同的内容
//   if (req.url === '/') {
//     res.end("首页");
//   } else if (req.url === '/about') {
//     res.end("关于");
//   }
// })

// server.listen(80, () => {
//   console.log("服务器运行中...");
// })


// // 翻译成 express

// const express = require('express');
// // 根据express创建一个服务器
// const app = express();
// // 根据不同的 url标识符 访问不同路径
// app.get('/', (req, res) => {
//   res.send("首页");
// })
// app.get('/about', (req, res) => {
//   res.send("关于");
// })
// // 给服务器指定端口 并启动服务
// app.listen(80, () => {
//   console.log('服务器运行中...1');
// })


// const express = require('express');

// const app = express();

// app.get('/', (req,res)=> {
//   res.send('早安,地球');
// })
// app.get('/about', (req, res) => {
//   console.log('关于');
// })

// app.listen(3000, () => {
//   console.log("运行中...")
// })

const express = require('express');

const app = express();

app.get('/', (req,res)=> {
  res.send('早安,地球');
})
app.get('/about', (req, res) => {
  console.log('关于');
})

app.listen(3000, () => {
  console.log("运行中...")
})