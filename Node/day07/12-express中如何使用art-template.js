// var express = require('express');
// var app = express();
// // 告诉exprss 我要使用的模板的后缀是html
// app.engine('html', require('express-art-template'));

// // 设置 模板的路径是 views这个路径
// app.set('views', './views')

// let students = [
//   {
//     name: '张三',
//     age: '18',
//     sex: "男",
//     hobby: '篮球'
//   }, {
//     name: '张三1',
//     age: '18',
//     sex: "男",
//     hobby: '养马'
//   }, {
//     name: '张三2',
//     age: '18',
//     sex: "男",
//     hobby: '汤头'
//   }, {
//     name: '张三3',
//     age: '18',
//     sex: "男",
//     hobby: '抽烟'
//   }, {
//     name: '张三4',
//     age: '19',
//     sex: "女",
//     hobby: '篮球'
//   }
// ]
// app.get('/', (req, res) => {
//   // res.send("你好");
//   res.render('students.html', { students: students });
// })


// app.listen(80, () => {
//   console.log("服务器运行中");
// })

// // express中配置模板引擎
// // 1.下载
// // npm install --save art-template
// // npm install --save express-art-template

// // 2.配置模板的后缀
// // app.engine('html', require('express-art-template'));
// // 3.配置模板所在的路径
// // app.set('views', './views')    第一个views代表的是模板(不能改)  第二个view代表的是模板的路径
// // 4.把模板和数据拼接起来(也叫绑定起来)
// res.render('students.html', { students: students });
// res.render('模板名称', 数据对象(必须是对象));


















// // 引包
// var express = require('express');
// // 创建服务器
// var app = express();
// // 告诉exprss 我要使用的模板的后缀是html
// app.engine('html', require('express-art-template'));
// // 设置 模板的路径是 views这个路径
// app.set('views', './views')
// // 声明一个数组
// let students = [
//   {
//     name: 'guocheng',
//     age: '1213',
//     sex: "男",
//     hobby: '吃喝睡',
//     say:'宝塔镇河妖'
//   }, {
//     name: 'guocheng1',
//     age: '12',
//     sex: "男",
//     hobby: '睡觉',
//     say:'宝塔镇河妖'
//   }, {
//     name: 'guochengg',
//     age: '123',
//     sex: "男",
//     hobby: '打豆豆',
//     say:'宝塔镇河妖'
//   }, {
//     name: 'guochengg2',
//     age: '1',
//     sex: "男",
//     hobby: '抽烟',
//     say:'宝塔镇河妖'
//   }, {
//     name: 'guochengg3',
//     age: '1',
//     sex: "女",
//     hobby: '啊哈哈',
//     say:'宝塔镇河妖'
//   }
// ]
// app.get('/', (req, res) => {
//   // res.send("早安,地球");
//   res.render('students.html', { students: students });
// })
// // 指定端口 启动服务
// app.listen(80, () => {
//   console.log("服务器运行中");
// })

// express中配置模板引擎
// 1.下载
// npm install --save art-template
// npm install --save express-art-template
// 2.配置模板的后缀
// app.engine('html', require('express-art-template'));
// 3.配置模板所在的路径
// app.set('views', './views')    第一个views代表的是模板(不能改)  第二个view代表的是模板的路径
// 4.把模板和数据拼接起来(也叫绑定起来)
// res.render('students.html', { students: students });
// res.render('模板名称', 数据对象(必须是对象));













// 引包
var express = require('express');
// 创建服务器
var app = express();
// 告诉exprss 我要使用的模板的后缀是html
app.engine('html', require('express-art-template'));
// 设置 模板的路径是 views这个路径
app.set('views', './views')
// 声明一个数组
let students = [
  {
    name: 'guocheng',
    age: '1213',
    sex: "男",
    hobby: '吃喝睡',
    say:'宝塔镇河妖'
  }, {
    name: 'guocheng1',
    age: '12',
    sex: "男",
    hobby: '睡觉',
    say:'宝塔镇河妖'
  }, {
    name: 'guochengg',
    age: '123',
    sex: "男",
    hobby: '打豆豆',
    say:'宝塔镇河妖'
  }, {
    name: 'guochengg2',
    age: '1',
    sex: "男",
    hobby: '抽烟',
    say:'宝塔镇河妖'
  }, {
    name: 'guochengg3',
    age: '1',
    sex: "女",
    hobby: '啊哈哈',
    say:'宝塔镇河妖'
  }
]
app.get('/', (req, res) => {
  // res.send("早安,地球");
  res.render('students.html', { students: students });
})
// 指定端口 启动服务
app.listen(80, () => {
  console.log("服务器运行中");
})
