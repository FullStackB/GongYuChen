// 下面的代码 明显 不太好
// 如果永乐票务的活动一直不变  但是如果天天变 我们难道每天修改html和数据吗 太麻烦 

// const http = require('http');

// const server = http.createServer();

// server.on('request', (req, res) => {
//   // res.end("你哈");

//   res.end(`<!DOCTYPE html>
//   <html lang="en">

//   <head>
//     <meta charset="UTF-8">
//     <meta name="viewport" content="width=device-width, initial-scale=1.0">
//     <meta http-equiv="X-UA-Compatible" content="ie=edge">
//     <title>Document</title>
//   </head>

//   <body>
//     <ul>
//       <li><a href="javascript">[上海] "Higher Brothers 2019恭喜发财 WISH YOU RICH 世界巡演"</a></li>
//       <li><a href="javascript">[天津] 2019"绝色"莫文蔚巡回演唱会—天津站</a></li>
//       <li><a href="javascript">[廊坊] 2019中国平安中超联赛第三轮 河北华夏幸福vs上海上港</a></li>
//       <li><a href="javascript">[武汉] 2019年中国·武汉速度赛马公开赛（3月）</a></li>
//     </ul>
//   </body>

//   </html>`)
// })


// server.listen(80, () => {
//   console.log("服务器运行中...")
// })


// ============================================================================================================




// 使用模板引擎更好 
// 模板引擎是什么 是相当于 冰棍和 模具的关系

// 常见的模板引擎有: 1.art-template   2.ejs  3.swig  4.pug   5.handlebars  6. baidutemplate

// arttempalte的使用
// 1.下包
// 2.引入包
// 3.使用  结果是一个拼接好的html页面 = template(模板的地址, 必须是一个对象)

const http = require('http');
const template = require('art-template');
const path = require('path');
const server = http.createServer();

let arr = [
  "[上海] \"Higher Brothers 2019恭喜发财 WISH YOU RICH 世界巡演\"",
  "2019\"绝色\"莫文蔚巡回演唱会—天津站",
  "[廊坊] 2019中国平安中超联赛第三轮 河北华夏幸福vs上海上港",
  "[武汉] 2019年中国·武汉速度赛马公开赛（3月",
  "[武汉] 2019年中国·武汉速度赛马公开赛（3月",
  "[武汉] 2019年中国·武汉速度赛马公开赛（3月"
]

server.on('request', (req, res) => {
  // res.end("你哈");
  let result = template(path.join(__dirname, './public/yongle.html'), { list: arr, title: "永乐票务" });
  // console.log(result);
  res.end(result);
})


server.listen(80, () => {
  console.log("服务器运行中...")
})

