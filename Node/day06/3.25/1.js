// 导入模块

const http = require('http')
const fs = require('fs')
const path = require('path')


// 创建
fs.writeFile('exam.html', '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><title>Document</title></head><body><h1>', () => {})

// 添加
fs.appendFile('./exam.html', '我叫王大锤,今年18岁,我的特点是能吃能喝能睡</h1></body></html>', () => {})

// 创建一个http服务器
const server = http.createServer()
// 绑定事件
server.on('request', (req, res) => {
  //    告诉浏览器如何解析                 utf-8解析
  res.writeHead(200, {
    "Content-Type": "text/html;charset=UTF-8"
  });


  // 当路径为http://127.0.0.1:7878/exam.html时 读取内容并显示
  if (req.url === '/') {
    res.end('欢迎光临')

  } else if (req.url === '/exam.html') {
    // 读取
    fs.readFile(path.join(__dirname, './exam.html'), 'utf8', (error, data) => {
      // 如果出错显示错误信息
      if (error) {
        console.log(error);
      }
      // 显示数据
      res.end(data);
    })
    // res.end('欢迎光临')
  } else {
    res.end('404,页面找不到')

  }
})
// 启动并监听
server.listen(7878, () => {
  console.log('访问:http://127.0.0.1:7878')
})