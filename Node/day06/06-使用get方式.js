// //1.引入包
// const express = require('express');
// const qs = require('querystring');
// // 2.创建服务器
// const app = express();


// // 设置静态资源目录
// app.use(express.static("public"));

// // 3.监听路径 返回响应
// app.get('/', (req, res) => {
//   res.end("首页");
// })

// // 根据前端的请求地址 接受前端的数据
// // 什么方式提交 就用什么方式接受

// // 如果你想接受前端用户输入的用户名和密码
// // 需要使用一个原生Node的模块 queryString
// app.get('/login', (req, res) => {
//   console.log(req.query);
//   // res.send(req.query.username + ",万岁万岁万万岁");
//   //  res.send(
//   // 登陆
//   // if (req.query.username == "admin" && req.query.password == "123456") {
//   //   res.send("欢迎回家:" + req.query.username);
//   // }

//   res.send(req.query.username + ', 您好,您猜一猜我知道你的密码不？ 哈哈哈,我知道您的密码是: ' + req.query.password);
// })

// // 4.指定端口 启动服务
// app.listen(3000, () => {
//   console.log("服务器运行中...")
// })




// const express = require('express');
// const qs = require('querystring');
// const app = express();
// app.use(express.static("public"));
// app.get('/', (req, res) => {
//   res.end("首页");
// })
// app.get('/login', (req, res) => {
//   console.log(req.query);
//   res.send(req.query.username + ', 您好,您猜一猜我知道你的密码不？ 哈哈哈,我知道您的密码是: ' + req.query.password);
// })
// app.listen(3000, () => {
//   console.log("服务器运行中...")
// })



//1.引入包
const express = require('express');
const qs = require('querystring');
// 2.创建服务器
const app = express();
// 设置静态资源目录
app.use(express.static("public"));
// 3.监听路径 返回响应
// 根据前端的请求地址 接受前端的数据
app.get('/', (req, res) => {
  res.end('服务器运行中');
})
// 什么方式提交 就用什么方式接受
// 需要使用一个原生Node的模块 queryString
// 如果你想接受前端用户输入的用户名和密码
app.get('/login', (req, res) => {
  console.log(req.query);
  res.send(req.query.username + ', 您好,您猜一猜我知道你的密码不？ 哈哈哈,我知道您的密码是: ' + req.query.password);

})
// 4.指定端口 启动服务
app.listen(3000, () => {
  console.log('服务器运行中')
})
