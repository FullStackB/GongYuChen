// //1.引入包
// const http = require('http');
// // 2.根据包创建服务器
// const server = http.createServer();
// // 3.监听客户端的request请求事件 并处理响应
// server.on('request', (req, res) => {
//   res.end("首页");
// })
// // 4.指定端口 启动服务
// server.listen(3000, () => {
//   console.log('服务器运行中...');
// })


// // 引入包
// const http = require('http');
// // 根据包创建服务器
// const seerver = http.createServer();
// // 监听客户端的请求
// server.on('request', (req, res) => {
//   res.end('hello world');
// })
// // 指定端口,开始服务
// server.listen(3000, () => {
//   console.log('运行中')
// })




// 引入包
const http = require('http');

const server = http.createServer();

server.on('request', (req, res) => {
  console.log('首页');
})

server.listen(3000, () => {
  console.log('服务器运行中')
})