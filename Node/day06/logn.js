
//1.引入包
const express = require('express');
const qs = require('querystring');
const fs = require('fs')
const path = require('path')

// 2.创建服务器
const app = express();
// 设置静态资源目录
app.use(express.static("student"));
// 3.监听路径 返回响应
// 根据前端的请求地址 接受前端的数据
app.get('/', (req, res) => {
  res.end('服务器运行中');
})
// 什么方式提交 就用什么方式接受
// 需要使用一个原生Node的模块 queryString
// 如果你想接受前端用户输入的用户名和密码
app.get('/login', (req, res) => {
  var arr = req.query;
  var arrStr = JSON.stringify(arr);
  // 将获取的信息写入data.json
  fs.appendFile(path.join(__dirname,'./data.json'),arrStr,'utf8' ,()=>{})
  fs.readFile(path.join(__dirname, './student/logon.html'), 'utf8', (error, data) => {
    // 如果出错显示错误信息
    if (error) {
      console.log(error);
    }
    // 显示数据
    res.end(data);
  })
})
// 4.指定端口 启动服务
app.listen(3000, () => {
  console.log('服务器运行中')
})
