//1. 什么是静态资源
// css html js 图片

// 2.为什么要托管静态资源呢？
// 在开发中 分前端(html css js 图片) 和 后端(php java python  Node)
// 前端代码让改  后端代码涉及到业务还有金融方面的问题不让用户看到
// 刚才我们看到的都是不让看  需要把前端代码释放出来
// 这时候就需要 托管静态资源了


// 3.怎样托管静态资源 http://expressjs.com/zh-cn/starter/static-files.html
// 在路径上不需要添加任何东西 以public为基准 选择文件即可 在use后面没有路径 那么用的时候 就 不需熬写路径
// app.use(express.static('public'));  

// 如果use后面有public 代表路径中必须包含/public  
// app.use('/public', express.static("public"));




// // 引入包
// const express = require('express');
// // 通过express 创建一个服务器
// const app = express();
// // 设置静态资源的目录 就可以了 
// // app.use(express.static('public'));
// app.use('/public', express.static("public"));
// // 根据不同的 url标识符  处理不同的请求
// app.get('/', (req, res) => {
//   // res.end("首页");
//   res.send(`<!DOCTYPE html>
//   <html lang="en">
  
//   <head>
//     <meta charset="UTF-8">
//     <meta name="viewport" content="width=device-width, initial-scale=1.0">
//     <meta http-equiv="X-UA-Compatible" content="ie=edge">
//     <title>Document</title>
//     <link rel="stylesheet" href="./public/css/main.css">
//   </head>
  
//   <body>
//     <h1>首页</h1>
//   </body>
  
//   </html>`);
// })

// // 给服务器指定端口 启动服务
// app.listen(80, () => {
//   console.log("请访问： http://127.0.0.1:80");
// })

// const express = require('express');
// const app = express();
// app.use('/public', express.static("public"));
// app.get('/', (req, res) => {
// res.send(`<!DOCTYPE html>
// <html lang="en">
// <head>
//   <meta charset="UTF-8">
//   <meta name="viewport" content="width=device-width, initial-scale=1.0">
//   <meta http-equiv="X-UA-Compatible" content="ie=edge">
//   <title>Document</title>
//   <link rel="stylesheet" href="/public/css/main.css">
// </head>
// <body>
//   <h1>首页</h1>
// </body>
// </html>`);
// })
// app.listen(3000, () => {
//   console.log("请访问： http://127.0.0.1:3000");
// })






// 引入
const express = require('express');
// 用express创建一个服务器
const app = express();
// 设置静态资源目录
app.use('/public', express.static("public"));
// 根据不同的url表示符 处理不同的请求
app.get('/', (req, res) => {
res.send(`<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="/public/css/main.css">
</head>

<body>
  <h1>早安,地球</h1>
</body>

</html>`);

})
// 给服务器 指定端口 并启动服务
app.listen(3000, () => {
  console.log("请访问： http://127.0.0.1:3000");
})