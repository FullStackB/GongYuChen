// //1.引入包
// const http = require('http');
// // 2.根据包创建服务器
// const server = http.createServer();
// // 3.监听客户端的request请求事件 并处理响应
// server.on('request', (req, res) => {
//   res.writeHead(200, { 'Content-Type': 'text/html;charset=UTF-8' });
//   res.end("首页");
// })
// // 4.指定端口 启动服务
// server.listen(3000, () => {
//   console.log('服务器运行中...');
// })

// 在开发中 如果每次更改服务器代码 都要手动重启服务器 非常的麻烦 
// 需要一个看到服务器代码更改了 就自动重启的工具这个工具叫做nodemon

// 1.全局安装nodemon
// npm install nodemon -g;
// 2.你要启动那个文件 就使用nodemon xxxx.js


// //1.引入包
// const http = require('http');
// // 2.根据包创建服务器
// const server = http.createServer();
// // 3.监听客户端的request请求事件 并处理响应
// server.on('request', (req, res) => {
//   res.writeHead(200, { 'Content-Type': 'text/html;charset=UTF-8' });
//   res.end("首页");
// })
// // 4.指定端口 启动服务
// server.listen(3000, () => {
//   console.log('服务器运行中...');
// })





// //引入包
// const http = require('http');
// //根据包创建服务器
// const server = http.createServer();
// // 监听客户端的请求
// server.on('request', (req, res) => {
//   res.writeHead(200, { 'Content-Type': 'text/html;charset=UTF-8' });
//   res.end("首页");
// })
// // 指定端口 启动服务
// server.listen(3000, () => {
//   console.log('服务器运行中...');
// })
