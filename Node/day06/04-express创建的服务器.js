//1. 为什么要学习express框架
// 因为方便 express框架已经为我们开发了很多有用的包  便于我们开发

// 2.什么是express框架
// 在node的原生方法的基础之上做了一层封装   高度包容、快速而极简的 Node.js Web 框架

// 3.express 和node的关系
// 相当于js和jquery的关系


// 4.express如何用
// 4.1 下载包
// 4.2 引入包
// 4.3 用包 http://expressjs.com/zh-cn/starter/hello-world.html


// // 引入包
// const express = require('express');
// // 通过express 创建一个服务器
// const app = express();
// // 根据不同的 url标识符  处理不同的请求
// app.get('/', (req, res) => {
//   // res.end("首页");
//   res.send('首页');
// })
// // 给服务器指定端口 启动服务
// app.listen(80, () => {
//   console.log("请访问： http://127.0.0.1:80");
// })



// // 引入
// const express = require('express');
// // 通过express创建一个服务器
// const app = express();
// // 根据不同的 url标识符 处理不同的请求

// app.get('/', (req, res) => {
//   res.send('早安,地球');
// })
// // 给服务器指定端口 开启服务
// app.listen(3000, () => {
//   console.log('请运行, http://127.0.0.1:3000')
// })

// 引入
const express = require('express');
// 通过express创建一个服务器
const app = express();
// 根据不同的 url标识符 处理不同的请求
app.get('/', (req, res) => {
  res.send('早安,地球');
})
// 给服务器指定端口 开启服务
app.listen(3000, () => {
  console.log('请运行, http://127.0.0.1:3000')
})