// 引包
const express = require("express");
// 创建服务器
const app = express();
// 配置post请求的 body-Parser
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
// 配置静态目录
app.use('/public', express.static('public'));
// 请求返回get.html
app.get('/', (req, res) => {
  res.sendFile('./views/post.html', { root: __dirname });
})
// 接受表单传来的数据
app.post('/user', (req, res) => {
  res.send(req.body);
})
app.get('/user', (req, res) => {
  res.send(req.query.username + '岁岁岁岁苏斯u苏')
})
// 启动服务 
app.listen(80, () => {
  console.log('服务器运行中')
})