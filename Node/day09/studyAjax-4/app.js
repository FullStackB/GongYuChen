// // 引入express框架
// const express = require('express');

// // 创建服务器
// const app = express();

// // 配置接受post请求的body-parser
// const bodyParser = require('body-parser');
// app.use(bodyParser.urlencoded({ extended: false }));

// // 配置静态目录
// app.use('/public', express.static('public'));

// // 配置模板
// // app.engine('html', require('express-art-template'));
// // // 设置模板目录
// // app.set('views', 'views');

// // 请求 / 的时候 返回 get.html
// app.get('/', (req, res) => {
//   res.sendFile('./views/post.html', { root: __dirname });
//   // res.sendFile(path.join(__dirname,'./views/get.html'));
// })

// // 接受表单传来的数据
// app.post('/user', (req, res) => {
//   // console.log();
//   // res.json([
//   //   {
//   //     name: "张三1",
//   //     age: 18,
//   //     sex: "女",
//   //     email: "12@qq.com"
//   //   },
//   //   {
//   //     name: "张三2",
//   //     age: 19,
//   //     sex: "女",
//   //     email: "13@qq.com"
//   //   },
//   //   {
//   //     name: "张三3",
//   //     age: 20,
//   //     sex: "女",
//   //     email: "14@qq.com"
//   //   },
//   //   {
//   //     name: "张三4",
//   //     age: 21,
//   //     sex: "女",
//   //     email: "15@qq.com"
//   //   }
//   // ])

//   res.json({ name: "藏三" });
// })

// app.get('/user', (req, res) => {
//   // console.log();
//   res.send(req.query.username + '爱的色放的萨芬的萨芬')
// })

// // 启动服务并指定端口
// app.listen(80, () => {
//   console.log("服务器运行中");
// })















// // 引入express框架
// const express = require('express');

// // 创建服务器
// const app = express();

// // 配置接受post请求的body-parser
// const bodyParser = require('body-parser');
// app.use(bodyParser.urlencoded({ extended: false }));

// // 配置静态目录
// app.use('/public', express.static('public'));

// // 配置模板
// // app.engine('html', require('express-art-template'));
// // // 设置模板目录
// // app.set('views', 'views');

// // 请求 / 的时候 返回 get.html
// app.get('/', (req, res) => {
//   res.sendFile('./views/post.html', { root: __dirname });
//   // res.sendFile(path.join(__dirname,'./views/get.html'));
// })

// // 接受表单传来的数据
// app.post('/user', (req, res) => {
//   res.json({ name: "藏三" });
// })
// app.get('/user', (req, res) => {
//   // console.log();
//   res.send(req.query.username + '爱的魔力转圈圈')
// })
// // 启动服务并指定端口
// app.listen(80, () => {
//   console.log("服务器运行中");
// })





























// 引入express框架
const express = require('express');
// 创建服务器
const app = express();
// 配置接受post请求的body-parser
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
// 配置静态目录
app.use('/public', express.static('public'));
// 请求 / 的时候 返回 get.html
app.get('/', (req, res) => {
  res.sendFile('./views/post.html', { root: __dirname });
})
// 接受表单传来的数据
app.post('/user', (req, res) => {
  res.json({ name: "管理员" });
})
app.get('/user', (req, res) => {
  // console.log();
  res.send(req.query.username + '爱的魔力转圈圈')
})
// 启动服务并指定端口
app.listen(80, () => {
  console.log("服务器运行中...");
})