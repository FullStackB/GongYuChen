﻿// 为什么要封装第三版
// 问题: 
  // 工程师a: function ajax(){}
  // 工程师b: funciton ajax(){}  
  // 出现的问题 最后无论其他怎么办 都会用b方案  (模块化会到后面学习)  在这个地方 我们先用一种简单的方式(命名空间)

//  问题:
  // 在正常情况下 我们所传的请求正文的格式: key=value&key=value  但是这样的需要自己拼接成http协议要求的格式,
  // 但是其实我们更多的是传入一个对象   怎么样把一个对象变成 key=value&key=value的样子
 //  username  password  email  tel  address  age  schoolNumber 身份证号  表单的数据特别多 需要直接传入对象

// $能不能是一个变量
var $ ={
  // 用来把对象变成 key=value&key=value这样的字符串
    param:function(obj){
      var str = "";
      // 遍历对象了  
      for (var key in obj) {
        // 
        str += key +'='+obj[key]+'&';
      }
      // name=itcast&age=21&
      // console.log(str);
      str =str.slice(0,-1);
      // console.log(str);
      return str;
    },
    ajax:function(option){
      // 提交方式
      var type = option.type || 'get';
      // 提交地址
      // location.pathname 就是本文件的相对路径
      var url = option.url || location.pathname;
      // 提交数据
      var data = this.param(option.data || {});
console.log(this)
      // 成功时的回调
      var success =option.success;

      // ajax请求
      var xhr =new XMLHttpRequest();

      if(type == 'get'){
        url = url +'?'+data;
        data =null;
      }
      xhr.open(type,url);

      if(type == 'post'){
        xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
      }

      xhr.send(data);
      xhr.onreadystatechange =function (){
        if(xhr.readyState == 4 && xhr.status == 200){
          success(xhr.responseText);
        }
      }
    }
}