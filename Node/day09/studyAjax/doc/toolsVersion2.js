// 第二版要解决的问题
//  1. ajax函数传参不够灵活  把ajax工具函数的参数变成对象
//  2. 要设置一些默认值      就把参数作为对象的属性 设置默认值

function ajax(option){
  // 设置默认参数
  // type 提交方式
  var type = option.type || 'get';
  // url  提交地址
  var url = option.url || '';
  // data 提交的数据
  var data = option.data || null;
  // success 成功时的回调
  var success = option.success;

  // 1. 实例化对象
  var xhr = new XMLHttpRequest();
  // 2. 设置请求行
  if(type == 'get'){
    url = url + "?" +data;
    data =null;
  }
  xhr.open(type,url);
  // 3. 设置请求头
  if(type == 'post'){
    xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
  }
  // 4. 设置请求正文
  xhr.send(data);
  // 5. 监听并处理相应
  xhr.onreadystatechange =function(){
    if(xhr.readyState== 4 && xhr.status ==200){
      success(xhr.responseText);
    }
  }
}