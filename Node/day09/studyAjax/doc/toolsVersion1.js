// 封装之前
// var xhr = new XMLHttpRequest();

// xhr.open()

// xhr.setRequestHeader();


// xhr.send();


// xhr.onreadystatechange=function(){
//     if(xhr.readyState== 4 && xhr.status == 200 ){
//       var result =xhr.responseText;
//     }
// }
//  要知道哪些参数
// 1. 请求方式 type
// 2. 请求地址 url
// 3. 请求参数 data
// 4. 成功时的回调 success

function ajax(type,url,data,success){
  // 1. 实例化对象
  var xhr = new XMLHttpRequest();
  // 2. 设置请求行
  if(type == 'get'){
    // get方式请求的格式应该 www.baidu.com ? username=itcast&password=123455
    url = url +'?'+data;
    data =null;
  }
  xhr.open(type,url);
  // 3. 设置请求头
  if(type == 'post'){
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  }
  // 4. 设置请求正文
  xhr.send(data)
  // 5. 监听并处理响应
  xhr.onreadystatechange = function () {
    if(xhr.readyState == 4 && xhr.status == 200){
        // 把我们的成功的结果当作实参返回给success这个回调函数
        success(xhr.responseText);
    }
  }
}