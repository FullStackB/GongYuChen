const express = require('express');

const app = express();
// 配置body-parser(尽量放在app的后面)
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }))

// 配置静态资源目录
app.use('/public', express.static('public'));
// 配置模板
app.engine('html', require('express-art-template'));
app.set('views', 'views');
// 配置路由
const router = require('./route');
app.use(router);

app.listen(80, () => {
  console.log("请访问: http://localhost");
})