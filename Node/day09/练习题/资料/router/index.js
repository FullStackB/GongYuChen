// 添加路由express框架
const express = require('express');
// 使用Router创建路由框架
const router = express.Router();
// 调用模块
const controller = require('../controller');
// 注册
router.get('/',controller.index);
// 添加数据
router.post('/adduser',controller.adduser);
// 展示
router.get('/show',controller.show);
// 删除数据
router.get('/deluser',controller.deluser);
router.get('/gai',controller.gai);
router.post('/update',controller.update);
// 把路由暴露出去
module.exports = router
