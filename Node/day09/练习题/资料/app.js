// 引入express模块
const express = require('express');
const app = express();
// 引入post需要的
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended:false}));
// 配置渲染环境
app.engine('html',require('express-art-template'));
//设置渲染目录
app.set('views','views');
app.use(express.static('views'));
// 引入路由
const router = require('./router');
//把路由挂到服务器上
app.use(router);
// 指向端口，启动服务器
app.listen(80, () => {
  console.log('启动成功 http://127.0.0.1:80');
});
