//ES6 允许在对象之中，直接写变量。这时，属性名为变量名, 属性值为变量的值。

let name = 'zs';
let age = 18;
let sex = "男"

// var obj = {
//   name: name,
//   age: age,
//   sex: sex,
//   sayHi: function () {
//     console.log(111);
//   }
// }
//  vue中简写比较多 的有属性和方法
// let obj = {
//   name,
//   age,
//   sex,
//   sayHi() {
//     console.log(111);
//   }
// }

// // console.log(obj);
// obj.sayHi();










//ES6 允许在对象之中，直接写变量。这时，属性名为变量名, 属性值为变量的值。

let name = 'js';
let age = 38;
let sex = "男"

// var obj = {
//   name: name,
//   age: age,
//   sex: sex,
//   sayHi: function () {
//     console.log(111);
//   }
// }
//  vue中简写比较多 的有属性和方法
// let obj = {
//   name,
//   age,
//   sex,
//   sayHi() {
//     console.log(111);
//   }
// }
// // console.log(obj);
// obj.sayHi();




//ES6 允许在对象之中，直接写变量。这时，属性名为变量名, 属性值为变量的值。

let name = 'gc';
let age = 18;
let sex = "老男人"

// var obj = {
//   name: name,
//   age: age,
//   sex: sex,
//   sayHi: function () {
//     console.log(111);
//   }
// }
//  vue中简写比较多 的有属性和方法
// let obj = {
//   name,
//   age,
//   sex,
//   sayHi() {
//     console.log(111);
//   }
// }

// // console.log(obj);
// obj.sayHi();