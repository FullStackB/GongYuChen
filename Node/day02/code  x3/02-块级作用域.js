/**
 * 为什么需要块级作用域
 * ES5只有全局作用域和函数作用域,没有块级作用域,这会带来很多不合理的场景
 * 1.内层变量可能会覆盖外层变量
 * 2.用来计数的循环遍历泄露为全局变量
 */


// 1.内层变量可能会覆盖外层变量
// 解释: 下面代码的原意是: if代码块的外部使用外层tmp变量,内部使用内层的tmp变量
// 但是,函数f执行后,输出结果为undefined,原因在于变量提升,导致内层的tmp变量覆盖了外层的tmp变量
// var tmp = new Date();

// function fn() {
//   console.log(tmp);
//   if (false) {
//     var tmp = "hello world";
//   }
// }

// fn();

// 2.用来计数的循环变量泄露为全局变量
// var s = 'hello';
// for (var i = 0; i < s.length; i++) {
//   console.log(s[i]);
// }

// console.log(i);


// 解决: let实际上为JavaScript新增了块级作用域

function fn() {
  let n = 5;
  if (true) {
    let n = 10;
  }
  console.log(n);
}

// 上面的函数有两个代码块,都声明了变量n,运行后输出5,这表示外层代码块不受内层代码块的影响,如果两次都是用var定义变量n
// 最后输出的值才是10


// 块级作用域的出现,实际上,使得广泛应用的立即执行函数表达式 不再必要了

// 块级作用域与函数声明
// 函数能不能在块级作用域之中声明呢？
// ES5规定,函数只能在顶层作用域和函数作用域之中声明,不能在块级作用域内声明
// 考虑到环境导致的行为差异太大,应该避免在块级作用域内声明函数,如果确实需要,
// 也应该写成函数表达式,而不是函数声明语句







// 1.内层变量可能会覆盖外层变量
// 解释: 下面代码的原意是: if代码块的外部使用外层tmp变量,内部使用内层的tmp变量
// 但是,函数f执行后,输出结果为undefined,原因在于变量提升,导致内层的tmp变量覆盖了外层的tmp变量
// var tmp = new Date();

// function fn() {
//   console.log(tmp);
//   if (false) {
//     var tmp = "hello world";
//   }
// }

// fn();

// 2.用来计数的循环变量泄露为全局变量
// var s = 'hello';
// for (var i = 0; i < s.length; i++) {
//   console.log(s[i]);
// }

// console.log(i);


// 解决:let实际上为JavaScript新增了块级作用域

function fn() {
  let n = 5;
  if (true) {
    let n = 10;
  }
  console.log(n);
}







// 1.内层变量可能会覆盖外层变量
// 解释: 下面代码的原意是: if代码块的外部使用外层tmp变量,内部使用内层的tmp变量
// 但是,函数f执行后,输出结果为undefined,原因在于变量提升,导致内层的tmp变量覆盖了外层的tmp变量
// var tmp = new Date();

// function fn() {
//   console.log(tmp);
//   if (false) {
//     var tmp = "hello world";
//   }
// }

// fn();

// 2.用来计数的循环变量泄露为全局变量
// var s = 'hello';
// for (var i = 0; i < s.length; i++) {
//   console.log(s[i]);
// }

// console.log(i);


// 解决:let实际上为JavaScript新增了块级作用域

function fn() {
  let n = 100;
  if (true) {
    let n = 10;
  }
  console.log(n);
}