
// 极端情况下,左侧只有一个形参,右侧只有一行代码;则左右的括号都可以省略;
// var fn = function (name) {
//   console.log(name);
// }
// fn('zs');

// var fn = name => console.log(name);
// fn('sz');

// 如果省略了右侧的{},则默认会把右侧代码的执行结果,当作函数的返回值,return出去
// var fn = () => 1;
// var result = fn();
// console.log(result);


// 如果某一行代码,是以[] () + - /五个符号中的任何一个开头,则应该在行首,添加一个分号


// 极端情况下,左侧只有一个形参,右侧只有一行代码;则左右的括号都可以省略;
// var fn = function (name) {
//   console.log(name);
// }
// fn('js');

// var fn = name => console.log(name);
// fn('sz');


// 如果省略了右侧的{},则默认会把右侧代码的执行结果,当作函数的返回值,return出去
// var fn = () => 1;
// var result = fn();
// console.log(result);


// 如果某一行代码,是以[] () + - /五个符号中的任何一个开头,则应该在行首,添加一个分号


// 极端情况下,左侧只有一个形参,右侧只有一行代码;则左右的括号都可以省略;
// var fn = function (name) {
//   console.log(name);
// }
// fn('js');

// var fn = name => console.log(name);
// fn('sz');


// 如果省略了右侧的{},则默认会把右侧代码的执行结果,当作函数的返回值,return出去
// var fn = () => 1;
// var result = fn();
// console.log(result);

// 如果某一行代码,是以[]()+-/五个符号中的任何一个开头,则应该在行首,添加一个分号
