/**
 * let 的基本用法
 * ES6新增了let命令,用来声明变量.
 * 它的用法类似于var,但是所声明的变量,只在let命令所在的代码块内有效
 */
// {
//   var name = 'suzumiya';
//   let age = 16;
//   console.log(age);
// }
// console.log(name);

// console.log(age);  
/**
 * let 不存在变量提升
 * var命令会发生'变量提升'的现象,即变量可以在声明之前使用,值为undefined.
 * 这种现象多多少少是有些奇怪的.
 * 按照一般的逻辑变量应该在声明语句之后才可以使用
 * 为了纠正这种现象,let命令改变了语法行为,它所声明的变量一定要在声明后使用,否则就报错
 */
// console.log(age);
// var age = 10;

// cosole.log(age);
// let age = 19;


// let age = 19;
// console.log(age);

/**
 * let不允许在相同的作用域内重复声明同一个变量
 * Identifier(标识符)'a' has already(已经) been declared(声明)
 */
// {
//   let a = 20;
//   // let a = 10;
//   console.log(a);
// }





/**
 * let的用法和var类似,但let声明的变量旨在了let命令所在的代码块内有效
 */
// {
//   var name = 'kon';
//   let age = 17;
//   console.log(age);
// }
// console.log(name);

/**
 * let不存在变量提升
 * var命令会发生变量提升现象 即 变量可以再声明前使用 ,值为undefined
 * let的变量必须 先声明再使用 否则就报错  
 */
// console.log(age);// undefined
// var age = 10;

// console.log(age);// age is defined
// let age = 10; 

// let age = 10;
// console.log(age);


/**
 * let 不允许在相同的作用域内重复声明同一个变量
 */
// {
//   // let a = 10;
//   let a = 20;
//   console.log(a)
// }





/*
 * let命令的用法和var命令类似,但let声明的变量只在let命令所在的代码块内有效
 */
// {
//   var name = 'wxy';
//   let age = 19;
//   console.log(age);
// }
// console.log(name);


/**
 * let不存在变量提升
 * var命令会发生变量提升现象 即 变量可以再声明前使用,值为undefined
 * let的变量必须 先声明再使用 否则就报错  
 */
// console.log(age);// undefined
// var age = 10;

// console.log(age);// age is defined
// let age = 10; 

// let age = 10;
// console.log(age);


/**
 * let 不允许在相同的作用域内重复声明同一个变量
 */
// {
//   let a = 10;
//   let a = 20;
//   console.log(a)
// }