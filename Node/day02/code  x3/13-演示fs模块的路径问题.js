// //读文件

// const fs = require('fs');

// // fs.readFile('./other/我的小郭程在吃鸡.txt', 'utf8', (error, data) => {
// //   if (error) {
// //     console.log(error);
// //   }

// //   console.log(data);
// // })

// // 在node中 最好不要用相对路径 因为相对路径都是以当前路径为开始的 
// // 要用绝对路径

// fs.readFile( '../other/我的小郭程在吃鸡.txt', 'utf8', (error, data) => {
//   if (error) {
//     console.log(error);
//   }

//   console.log(data);
// })



// //读文件

// const fs = require('fs');

// // fs.readFile('./other/我的小郭程在吃鸡.txt', 'utf8', (error, data) => {
// //   if (error) {
// //     console.log(error);
// //   }

// //   console.log(data);
// // })

// // 在node中 最好不要用相对路径 因为相对路径都是以当前路径为开始的 
// // 要用绝对路径

// fs.readFile( '../other/我的小郭程在吃鸡.txt', 'utf8', (error, data) => {
//   if (error) {
//     console.log(error);
//   }

//   console.log(data);
// })



//读文件

const fs = require('fs');

// fs.readFile('./other/我的小郭程在吃鸡.txt', 'utf8', (error, data) => {
//   if (error) {
//     console.log(error);
//   }

//   console.log(data);
// })

// 在node中 最好不要用相对路径 因为相对路径都是以当前路径为开始的 
// 要用绝对路径

fs.readFile( '../other/我的小郭程在吃鸡.txt','站着吃不着鸡', 'utf8', (error, data) => {
  if (error) {
    console.log(error);
  }

  console.log(data);
})