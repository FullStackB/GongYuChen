
//读文件 就是用代码读取文件中的内容信息
// node为我们提供了读文件的一个模块 fs(file system 文件系统)

// node的组成: ECMAScript 全局对象 核心API
// 如果想要用核心api 你必须知道这个api属于那个模块
// 如果不是node内置的 三步: 下包 引包 用包
// 如果是node内置的 两步:  

// 1.引包
const fs = require('fs');


// 2.用包
// console.log(fs);

// fs.readFile(要读的文件地址,可选读文件的编码, function(错误信息, 读出来的数据){})

// fs.readFile('./file/1.txt', (error, data) => {
//   if (error) {
//     console.log(error.message);// 如果有错误 请打印错误信息
//   }

//   console.log(data.toString());
// })


// fs.readFile('./file/1.txt', 'utf8', (error, data) => {
//   if (error) {
//     console.log(error.message);// 如果有错误 请打印错误信息
//   }

//   console.log(data);
// })


// 1.引包
const fs = require('fs');

// 2.用包
fs.writeFile('./other/other.txt', 'I 💗  human', (err) => {
  console.log('victory')
})
fs.readFile('./other/other.txt', (error, data) => {
  if (error) {
    console.log(error.message);
  }
  console.log(data.toString());
})

// 1.引包
const fs = require('fs');

// 2.用包
fs.writeFile('./other/other.txt', 'I 💗  human', (err) => {
  console.log('victory')
})
fs.readFile('./other/other.txt', (error, data) => {
  if (error) {
    console.log(error.message);
  }
  console.log(data.toString());
})