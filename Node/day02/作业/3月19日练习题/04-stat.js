// 4、使用node.js读取文件信息
// 4.1、首先对文件‘04-stat.txt’写入内容“全栈谁最牛”；
// 4.2、其次对文件‘04-stat.txt’追加内容“我最牛”；
// 4.3、获取文件‘04-stat.txt’大小 单位是字节；
// 4.4、获取文件‘04-stat.txt’的创建时间
// 4.5、判断‘04-stat.txt’是否是一个文件
// 4.6、判断‘04-stat.txt’是不是目录
// 4.7、请在04-stat.js中完成

const fs = require('fs');

fs.writeFile(__dirname+'/04-stat.txt', '全栈谁最牛', 'utf8', (error) => {
  if (error) {
    console.log(error);
  }
});
fs.appendFile(__dirname+'/04-stat.txt', '我最牛', 'utf8', (error) => {
  if (error) {
    console.log(error);
  }
});
fs.stat(__dirname+'/04-stat.txt', 'utf8', (error,stat) => {
  if (error) {
    console.log(error.message);
  }
  // 大小
  console.log(stat.size);
  // 创建时间
  console.log(stat.birthtime.toString());
  // 是不是文件
  console.log(stat.isFile());
});
// 读取
fs.readdir(__dirname, 'utf8', (err, files) => {
  if (err) {
    console.log(err);
  }
  console.log(files);
})
