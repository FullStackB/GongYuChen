
/** 
 * 3、使用node.js完成如下功能
 * 3.1、首先对文件‘03-write.txt’写入内容“我是传智专修学院的大学生，我要好好学习”；
 * 3.2、其次对文件‘03-write.txt’追加内容“我是最棒的”；
 * 3.3、最后读取“03-write.txt”整个文件内容显示到控制台；
 * 3.4、请在03-appendFile.js中完成
*/

//1.引包
const fs = require('fs');
// 写入
fs.writeFile(__dirname+'/03-write.txt', '我是传智专修学院的大学生，我要好好学习', 'utf8', (error) => {
  if (error) {
    console.log(error);
  }
});
// 追加
// fs.appendFile(文件地址, 追加的数据, 编码格式, 回调函数)

fs.appendFile(__dirname+'/03-write.txt', '我是最棒的', 'utf8', (error) => {
  if (error) {
    console.log(error);
  }

  // console.log('追加文件成功了');
  
});
// const fs = require('fs');
// 读取
fs.readFile(__dirname+'/03-write.txt', 'utf8', (error, data) => {
  if (error) {
    console.log(error);
  }
  console.log(data);
});
// asynchronous
// 让程序员去买菜,买一个西瓜,如果看见西红柿,就买两个.会买回来什么?          

