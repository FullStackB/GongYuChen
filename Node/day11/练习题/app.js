// 导入express框架
let express = require("express");
// 初始化express
let app = express();
// 处理post请求
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({
	extended: false
}));
// 设置模板引擎相关信息
let ejs = require("ejs");
let path = require("path");
//目录
app.set("views", './views');
// 定义模板引擎
app.engine("html", ejs.__express);
// 模板引擎
app.set("view engine", "html");
// 设置静态资源的访问
app.use("/public", express.static(__dirname + "/public"));
// 导入路由
let adminRouter = require("./routers/admin");
app.use('/', adminRouter);
// 监听服务器
app.listen(7788, function () {
	console.log("http://127.0.0.1:7788");
});