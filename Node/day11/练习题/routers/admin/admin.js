let express = require("express");

let router = new express.Router();

const mysql = require("../../config/db.js");

const crypto = require('crypto');
// 导入moment模块
const moment = require("moment");

// 管理员管理首页
router.get('/', function (req, res, next) {
	// 从数据库中查询数据
	mysql.query('select * from user', (error, result) => {
		// 判断是否执行成功
		if (error) {
			console.log(error);
		} else {
			// res.send("管理员管理首页");// 加载页面
			res.render('admin/admin/index.html', {
				data: result,
				search: ''
			});
		}
	})
});

// 管理员管理添加页面
router.get('/add', function (req, res, next) {
	// res.send("管理员管理添加页面");
	res.render('admin/admin/add.html');
	// 加载页面
});
// 管理员的添加功能
router.post("/add", function (req, res, next) {
	// 判断是否执行
	mysql.query('insert into user(username,password) values("' + req.body.username + '","' + req.body.password + '")', (error, result) => {
		if (error) {
			console.log(error)
		} else {
			// 从数据库中查询数据
			mysql.query("select * from user", (error, result) => {
				// 判断是否执行成功
				if (error) {
					console.log(error)
				} else {
					// 加载页面
					res.render("admin/admin/index.html", {
						data: result,
						search: ""
					})
				}
			})
		}
	})
});

// 无刷新修改状态
router.get("/ajax_status", function (req, res, next) {
	// 接受对应的数据
	//数据读取
	mysql.query('select * from user', (error, result) => {
		if (error) {
			console.log(error);
		} else {
			let newArr = result.filter((ele, index) => {
				return ele.username.indexOf(req.query.search) != -1;
			});
			res.render('admin/admin/index.html', {
				data: newArr,
				search: req.query.search
			});
		}
	}) 
})

// 管理员管理修改页面
router.get('/edit', function (req, res, next) {
	// 查询对应数据
	//数据读取
	mysql.query('select * from user where id=' + req.query.id, (error, result) => {
		if (error) {
			console.log(error);
		} else {
			res.render('admin/admin/edit.html', {
				data: {
					id: result[0].id,
					username: result[0].username
				}
			});
		}
	})
});

// 管理员数据修改功能
router.post("/edit", function (req, res, next) {
	// // 更改数据
	mysql.query('update user set username =("' + req.body.username + '"),password = ("' + req.body.password + '") where id =("' + req.body.id + '")', (error, result) => {
		if (error) {
			console.log(error);
		} else {
			res.redirect('/admin/');
		}
	}) // ----修改数据
});

// 无刷新删除数据
router.get("/ajax_del", function (req, res, next) {
	// 删除数据
	mysql.query('delete from user where id=' + req.query.id, (error, result) => {
		// 判断是否执行成功
		if (error) {
			console.log(error);
		} else {
			res.json({
				code: '10000'
			});
		}
	})
})

module.exports = router;