// 引入mysql数据库包
const mysql = require('mysql');

// 创建数据库连接
const connection = mysql.createConnection({
  host: '127.0.0.1',
  port: '3306',
  user: 'root',
  password: 'root',
  database: 'gyc'
})


module.exports = connection;

