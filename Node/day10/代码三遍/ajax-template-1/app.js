// 引包
const express = require('express');
// 创建服务器
const app = express();
// 配置body-parser
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
// 路由
const router = require('./route');
app.use(router);
// 配置模板
app.engine('html', require('express-art-template'));
app.set('views', 'views');
// 配置静态资源目录
app.use(express.static('./public'));
// 启动服务器
app.listen(80, () => {
  console.log("请访问: http://127.0.0.1:80");
})