// 引入express包
const express = require('express');
// 通过express的Router方法来创建路由包
const router = express.Router();
// 引入控制器
const controller = require('../controller')
// 显示首页
router.get('/', controller.index);
// 添加用户
router.post('/createUser', controller.createUser);
// 展示用户
router.get('/show', controller.showUser);
// 删除用户
router.get('/delUser', controller.delUser);
// 暴露给路由
module.exports = router;