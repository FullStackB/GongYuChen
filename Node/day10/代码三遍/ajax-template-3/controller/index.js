// 引入数据库连接
const connection = require('../data');
// 首页展示
module.exports.index = (req, res) => {
  // 如果index.html中需要前端渲染 切不可使用render
  res.render('index.html');
}
// 添加用户
module.exports.createUser = (req, res) => {
  // 使用数据库连接的query方法 添加用户
  connection.query('insert into user(username, password)values("'
    + req.body.username + '","' + req.body.password +
    '")', (error, result) => {
    if (error) {
      console.log(error);
    } else {
      // 检测正确与否 主要看两个地方 返回的result是否是正确的返回 数据库中有没有数据
      if (result) {
        res.json({
          code: '10000',
          msg: "添加用户成功"
        })
      }
    }
  })
}
// 查询用户
module.exports.showUser = (req, res) => {
  connection.query('select * from user', (error, result) => {
    if (error) {
      console.log(error);
    } else {
      console.log(result);
      res.json(result);
    }
  })
}
// 删除用户
module.exports.delUser = (req, res) => {
  connection.query('delete from user where id='
    + req.query.id, (error, result) => {
    if (error) {
      console.log(error);
    } else {
      // console.log(result);
      res.json({
        code: '10001',
        msg: '删除成功'
      })
    }
  })
}