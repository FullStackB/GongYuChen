// 3、使用node.js完成根据不同url显示不同的内容，要求
// 3.1、开启服务，并且使用8899端口；
// 3.2、访问”http://127.0.0.1:8899/home.html”显示“首页”
// 3.3、访问”http://127.0.0.1:8899/study.html”显示“学习”；
// 3.4、访问”http://127.0.0.1:8899/us.html”显示“我们”；
// 3.5、访问其他url显示“页面找不到”；

// 1. 导入http模块
const http = require('http');
// 2. 使用http模块创建服务器
const server = http.createServer();
// 3. 监听请求
server.on('request', (req, res) => {
    res.writeHead(200, {
        "Content-Type": "text/plain;charset=UTF-8"
    });
    if (req.url === '/' || req.url === '/home.html') {
        res.end("首页");
    } else if (req.url === '/study.html') {
        res.end("学习");
    } else if (req.url === '/us.html') {
        res.end("我们");
    } else {
        res.end("页面找不到");
    }
})
// 4. 指定端口
server.listen(8899, () => {
    console.log("请访问: http://127.0.0.1:8899");
})