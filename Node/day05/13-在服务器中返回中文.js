// // 1.导入http模块
// const http = require('http');
// // 2.使用http模块创建服务
// const server = http.createServer();
// // 3.监听请求事件
// server.on('request', (rep, res) => {
//   res.writeHead(200, { "Content-Type": "text/plain;charset=UTF-8" });
//   res.end("我是我自己的");
// })
// // 4.监听端口 
// server.listen(3003, () => {
//   console.log("请访问:http://127.0.0.1:3003")
// })




// // 导入http模块
// const http = require('http');
// // 使用http模块创建服务
// const server = http.createServer();
// // 监听请求事件
// server.on('request', (req, res) => {
//   // 在服务器中 要给前端返回数据 有时候会成功
//   res.writeHead(200, { "Content-Type": "text/plain;charset=UTF-8" });
//   res.end('你好，世界');
// })
// // 监听端口
// server.listen(3002, () => {
//   console.log("http://127.0.0.1:3002")
// })







// 导入http模块
const http = require('http');
// 使用http模块创建服务
const server = http.createServer();
// 监听请求事件
server.on('request', (req, res) => {
  // 在服务器中 要给前端返回数据 有时候会成功
  res.writeHead(200, { "Content-Type": "text/plain;charset=UTF-8" });
  res.end('你好，世界');
})
// 监听端口
server.listen(3002, () => {
  console.log("http://127.0.0.1:3002")
})