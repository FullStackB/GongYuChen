//1.导入http模块
const http = require('http');
const fs = require('fs');
const path = require('path');
// 2.使用http模块创建服务器
const server = http.createServer();
// 3.监听请求
server.on('request', (req, res) => {
  res.writeHead(200, { "Content-Type": "text/html;charset=UTF-8" });
  // console.log(req.url);
  if (req.url === '/' || req.url === "/index.html") {
    fs.readFile(path.join(__dirname, './index.html'), 'utf8', (error, data) => {
      if (error) {
        console.log(error);
      }
      res.end(data);
    })
    //  在地址栏输入  如果是/about.html 
  } else if (req.url === "/about.html") {
    fs.readFile(path.join(__dirname, './about.html'), 'utf8', (error, data) => {
      if (error) {
        console.log(error);
      }
      res.end(data);
    })
  }
})
// 4.指定端口
server.listen(8899, () => {
  console.log("请访问http://127.0.0.1:8899");
})