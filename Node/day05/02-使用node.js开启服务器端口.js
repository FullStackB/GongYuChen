// 2、使用node.js开启服务器端口,要求
// 2.1、端口号：8899
// 2.2、并且在页面输出：Hello 传智学院

// 1. 导入模块
const http = require('http');
// 2. 使用http模块创建一个服务器
const server = http.createServer();
// 3. 通过服务器的绑定事件的方法 
server.on('request', (req, res) => {
    res.writeHead(200, {
        "Content-Type": "text/plain;charset=UTF-8"
    });
    res.end('Hello 传智学院');
})
// 4.监听端口
server.listen(8899, () => {
    console.log("请访问: http://127.0.0.1:8899");
})