const add = require('./lib/add');
const minus = require('./lib/minus');
const cheng = require('./lib/cheng');
const chu = require('./lib/chu');
const yu = require('./lib/yu');

module.exports = {
  add,
  minus,
  cheng,
  chu,
  yu
}