// 读取package-lock.json文件,并将内容打印在控制台上，使用readFileSync读取，要求：
// 2.1-将读取到的数据转换为对象打印在控制台
// 2.2-将对象再次转换为字符串再次打印在控制台
// 2.3-通过【wirteFileSync】写入一个新的文件 newJson.txt

// 引包
const fs = require('fs');
// 读取并转化为对象
var result = JSON.parse(fs.readFileSync('./package-lock.json', 'utf8'));
//判别是不是对象
console.log(result instanceof Object);
// 打印到控制台
console.log(result);
// 转化为字符串
var resultStr = JSON.stringify(result);
// 类型
console.log(typeof resultStr);
//输出
console.log(resultStr);


fs.writeFileSync('./newJson.txt', result, 'utf8', (error, data) => {
  if (error) {
    console.log(error);
  }
})




