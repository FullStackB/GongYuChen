// 1.为什么需要json
// 答: json存在的意义在于后端给前端返回的数据大部分(90%)都是json

// 2.json是什么？
//  是一种轻量级的数据交换格式。 易于人阅读和编写。 同时也易于机器解析和生成
// json是符合javascript对象或数组格式的字符串

// var jsonArr = '[1,2,3,4,5]';
// var jsonObj = '{ name: "zhangsan", age: "18" }';

// 3.怎么证明这个数据是json？
// 网络中的数据都是字符串
// 但是前端需要把这些字符串转换成 数组或对象
// 因为 我要获取数组中的数据 那就要把这些复杂的字符串变成对象或数组
// [
//   {
//     "RMKEYS": "林宥嘉",
//     "MRMS": "https://www.228.com.cn/s/%E6%9E%97%E5%AE%A5%E5%98%89/"
//   },
//   {
//     "RMKEYS": "陈小春",
//     "MRMS": "https://www.228.com.cn/s/%E9%99%88%E5%B0%8F%E6%98%A5/"
//   },
//   {
//     "RMKEYS": "哈姆雷特",
//     "MRMS": "https://www.228.com.cn/s/%E5%93%88%E5%A7%86%E9%9B%B7%E7%89%B9/"
//   },
//   {
//     "RMKEYS": "许巍",
//     "MRMS": "https://www.228.com.cn/s/%E8%AE%B8%E5%B7%8D/"
//   },
//   {
//     "RMKEYS": "汪峰",
//     "MRMS": "https://www.228.com.cn/s/%E6%B1%AA%E5%B3%B0/"
//   }
// ]

// 把以下字符串变成能不能直接变成数组或对象呢
var jsonArr = "[1,2,3,4,5]";
var jsonObj = '{"name": "zhangsan",  "age": 18}';

// JSON.parse 把字符串变成对象或数组
console.log(typeof jsonArr);
var arr = JSON.parse(jsonArr);
// 判断是不是一个数组
console.log(Array.isArray(arr));

var obj = JSON.parse(jsonObj);
// 判别是不是一个对象
console.log(obj instanceof Object);

// 把对象或数组变成json字符串
// JSON.stringify(数组或对象)
// var arr = ['a', 'b', 'c', 'd'];
// var obj = {
//   name: 'zhangsan',
//   age: 19
// }

// var arrStr = JSON.stringify(arr);
// console.log(typeof arrStr);

// var objStr = JSON.stringify(obj);
// console.log(typeof objStr);


// 总结:
// 1.为什么要用json
// 2.json是什么
// 3.json转换为对象或数组 JSON.parse
// 4.对象或数组转换为json字符串 JSON.stringify
// 5.JSON字符串的格式: 如果是对象 那么对象的属性和值都要用双引号
// 6.JSON是字符串 但是也可以写成json文件