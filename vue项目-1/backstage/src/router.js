import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [{
      path: "/",
      name: "home",
      component: Home
    }, {
      path: "/home",
      component: Home,
      children: [{
          path: "/home",
          redirect: "/welcome"
        },
        {
          path: "/welcome",
          // 懒加载 组件 好处是 用的时候才去引入组件
          component: () => import("@/components/index.vue")
        },
        //  用户列表
        {
          path: "/users",
          // component: () => import("@/components/UserList.vue")
          component: () => import("@/components/UserList copy.vue")
        },
        //  角色列表
        {
          path: "/roles",
          component: () => import("@/components/RolesList.vue")
        },
        //  权限列表
        {
          path: "/rights",
          component: () => import("@/components/RightsList.vue")
        },
        //  商品列表
        {
          path: "/goods",
          component: () => import("@/components/GoodsList.vue")
        },
        //  商品添加列表
        {
          path: "/add",
          component: () => import("@/components/AddGoodsList.vue")
          // component: () => import("@/components/GoodsAdd.vue")
        },

        //  商品参数列表
        {
          path: "/params",
          component: () => import("@/components/ParamsList.vue")
        },
        //  商品分类列表
        {
          path: "/categories",
          component: () => import("@/components/categoryList.vue")
        },
        //  商品分类列表
        {
          path: "/orders",
          component: () => import("@/components/OrderList.vue")
        }
      ]
    },
    {
      path: "/login",
      name: "about",
      component: () =>
        import("./views/Login.vue")
    }
  ]
});
// 路由导航守卫
router.beforeEach((to, from, next) => {
  if (to.path == "/login") return next();

  const token = window.sessionStorage.getItem("token");
  if (!token) {
    window.sessionStorage.removeItem("token");
    return next("/login");
  }
  next();
})

export default router;