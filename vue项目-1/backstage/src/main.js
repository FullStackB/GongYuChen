import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import VueQuillEditor from "vue-quill-editor"

import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
Vue.use(VueQuillEditor);
// 配置elementUI
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(ElementUI);
// 引入全局基础css
import "./assets/css/base.css";
Vue.config.productionTip = false;
// 配置vue-with-table-grid插件
import tableTree from 'vue-table-with-tree-grid'

Vue.component("table-tree", tableTree);
// 配置axios
import Axios from "axios";
Vue.prototype.$http = Axios;
Axios.defaults.baseURL = "http://127.0.0.1:8888/api/private/v1/";

// 请求拦截器 
// axios发起的请求都要通过拦截器 要获取token
Axios.interceptors.request.use(function (config) {
  config.headers.Authorization = window.sessionStorage.getItem("token");
  return config;
},
function (error) {
    return Promise.reject(error);
});
Vue.filter('dateFormat', (original) => {
  let date = new Date(original);
  let year = date.getFullYear();
  let month = (date.getMonth() + 1).toString().padStart(2, '0');
  let day = date.getDate().toString().padStart(2, '0');
  let hour = date.getHours().toString().padStart(2, '0');
  let minutes = date.getMinutes().toString().padStart(2, '0');
  let seconds = date.getSeconds().toString().padStart(2, '0');
  return `${year}-${month}-${day} ${hour}:${minutes}:${seconds}`
})
new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
