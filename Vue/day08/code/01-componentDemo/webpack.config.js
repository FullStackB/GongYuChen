
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

// 导入插件
const VueLoaderPlugin = require('vue-loader/lib/plugin')
// new 一个插件的实例对象
const vuePlugin = new VueLoaderPlugin()
// 导出配置
module.exports = {
  // 设置webpack打包模式
  mode: "development",
  // 设置入口文件的
  entry: "./src/js/index.js",
  // 设置输出文件
  output: {
    // 文件的名字
    filename: "index.js",
    path: path.join(__dirname, "./dist/")
  },
  // 配置webpack-dev-server
  devServer: {
    contentBase: './dist/'
  },
  // 配置loader
  module: {
    rules: [
      // 处理各种各样文件的loader

      // 1.处理css的loader style-loader css-loader
      { test: /\.css$/, use: ['style-loader', 'css-loader'] },
      // 2.处理图片的loader  file-loader url-loader
      { test: /\.jpg|png|gif|bmp$/, use: 'url-loader?limit=20293' },
      // 3.处理 vue文件
      { test: /\.vue$/, use: 'vue-loader' }
    ]
  },
  // 配置插件
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: "index.html"
    }),
    new CleanWebpackPlugin(),
    vuePlugin
  ]
}