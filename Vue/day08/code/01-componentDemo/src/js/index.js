// 引入vue库文件
import Vue from "vue/dist/vue"

// 引入组件
import father from "../components/father.vue";

// 注册组件
Vue.component('father', father);


const vm = new Vue({
  el: '#app'
})