const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// 导入插件
// 导入插件
const VueLoaderPlugin = require('vue-loader/lib/plugin')
// new 一个插件的实例对象
const vuePlugin = new VueLoaderPlugin()
// 导出配置
module.exports = {
  mode: 'development',
  // 入口文件
  entry: './src/js/index.js',
  // 输出目录和文件
  output: {
    // 输出的文件名字
    filename: 'index.js',
    path: path.join(__dirname, "./dist/")
  },
  // 配置webpack-dev-server 启动路径
  devServer: {
    contentBase: './dist'
  },
  module: {
    rules: [
      // 处理css的
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      // 处理图片的
      // limit控制base64 和url
      // 当limit的值 小于图片大小 用base64
      // 当limit的值大于等于图片大小 用图片路径
      { test: /\.jpg|png|gif|bmp$/, use: 'url-loader?limit=20293' },
      // 处理字体文件的
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          'file-loader'
        ]
      },
      // bable作用就是把es6转成es3 和es5
      // clude包含  exclude排除
      { test: /\.js$/, use: 'babel-loader', exclude: /node_modules/ },
      // 处理.vue文件的
      { test: /\.vue$/, use: 'vue-loader' }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      // 源文件的html的名字
      template: './src/index.html',
      // 输出到dist目录的文件名字
      filename: 'index.html'
    }),
    vuePlugin
  ]
}