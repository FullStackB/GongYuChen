// 引入vue库文件
import Vue from "vue/dist/vue"

// 引入组件
import father from "../components/father.vue";

// 注册组件
Vue.component('father', father);


const vm = new Vue({
  // el: '#app',
  // // template: "<h1>我是template渲染的数据</h1>",
  // render: function (createElement) {
  //   return createElement(father);
  // }

  // render: function (c) {
  //   return c(father);
  // }

  render: h => h(father)

}).$mount('#app')