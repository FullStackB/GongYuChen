const fs = require('fs');

function readFileByPath(fpath){
  return  new Promise(function(resolve,reject){
        fs.readFile(fpath,'utf-8',(error,result)=>{
            if(error) return reject(error)
            resolve(result)
        })
    })
}

async function readFile3(){
    let p = await readFileByPath('./file/1.txt')
    console.log(p)
}