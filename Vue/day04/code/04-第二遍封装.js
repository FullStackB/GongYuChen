const fs = require('fs');

function readFileByPath(fpath){
    const p = new Promise(function(resolve,reject){
        fs.readFile(fpath,'utf-8',(error,result)=>{
            if(error) return reject(error)
            resolve(result)
        })
    })
    return p
}

readFileByPath('./file/1.txt').then(
    function(result){
        console.log(result)
       return readFileByPath('./file/2.txt');
    }).then(function(result){
        console.log(result)
        return readFileByPath('./file/3.txt');
    }).then(function(result){
        console.log(result)
    })// 以上均为成功信息
    // 以下为错误信息
    .catch(function(e){
        console.log(e.message)
    })