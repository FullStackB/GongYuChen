const fs = require('fs');

function readFileByPath(fpath){
    const p = new Promise(function(successCb,errorCb){
        fs.readFile(fpath,'utf-8',(error,result)=>{
            if(error) return errorCb(error)
            successCb(result)
        })
    })
    return p
}

const result = readFileByPath('./file/3.txt')

result.then(
    function(result){
        console.log('读取文件成功:'+result)
    },
    function(error){
        console.log('错误消息:'+ error)
    }
)