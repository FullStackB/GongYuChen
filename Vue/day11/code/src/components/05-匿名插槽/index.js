// 引入vue 和vue-router
import Vue from 'vue';

// 引入app.vue
import app from './app.vue';

// 实例化Vue
new Vue({
  el: '#app',
  // 渲染主组件
  render: h => h(app)
})