//在webpack中打包 实现animate.css动画

// 1.引入vue
import Vue from "vue/dist/vue";


// 创建Vue的全局组件
// Vue.component("组件名称", {vue组件的配置项})

// 一个页面就是用一个个的组件拼接起来的 既然是组件 那么一定有html  
// 在组件模板中 任何元素都可以作为根元素 只要暴露的是只有一个元素 但是不要钻牛角尖
// html body head 不能
Vue.component('my-com', {
  template: `
  <div>
    <h1 @click="handleClick">{{h1Msg}} +aaa</h1>
    <div>
      <h1>{{ divMsg}}  +bbb</h1>
    </div>
  </div>
  `,
  // 组件中的data有一点特殊 组件中的data必须是function 而且必须return一个字面量对象
  data: function () {
    return {
      h1Msg: "我是全局组件",
      divMsg: "我是被div包裹的h1"
    }
  },
  // methods 和实例对象的methods是完全一样
  methods: {
    handleClick: function () {
      console.log("我是全局组件点击之后的效果")
    }
  },
  created: function () {
    console.log(this.h1Msg);
    console.log("我是组件的声明周期函数created");
  }
})

// 使用组件: 只要是全局组件 那么就可以 把组件名使用标签的形式引入到html中就可以了

// 2.vue的实例化 组件是特殊的vue实例 那么vue实例有的 那么组件是不是都应该有
// 组件有template  data methods   声明周期函数 filter
// vue实例有 data methods 声明周期函数 filter
const vm = new Vue({
  el: '#app'
})
