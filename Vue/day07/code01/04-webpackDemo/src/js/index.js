//在webpack中打包 实现animate.css动画

// 1.引入vue
import Vue from "vue/dist/vue";

// 全局组件
let o = { count: 0 }
Vue.component('my-count', {
  template: `
  <div>
    <button @click="handleAdd">+1</button>
    <h3>当前的值是{{count}}</h3>
  </div>
  `,
  data: function () {
    // return {
    //   count: 0
    // }
    return o;
  },
  methods: {
    handleAdd: function () {
      this.count++;
    }
  }
})


// 第一个实例
const vm = new Vue({
  el: "#app"
})


// 第二个实例
const vm2 = new Vue({
  el: "#app2"
})

