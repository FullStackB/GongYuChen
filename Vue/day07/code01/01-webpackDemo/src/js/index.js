// 因为需要使用jquery实现隔行变色
import $ from 'jquery';
// 因为要使用css 所以要引入
import '../css/main.css';


$('li:odd').css("backgroundColor", 'yellow');
$('li:even').css("backgroundColor", 'red');


// 转换es6
class Person {
  static names = {a:"扎根",b:'as'};
}

console.log(Person.names)