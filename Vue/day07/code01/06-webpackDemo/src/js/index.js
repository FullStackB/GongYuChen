//在webpack中打包 实现animate.css动画

// 1.引入vue
import Vue from "vue/dist/vue";
import myCount from '../components/myCount.vue';
// 全局组件
// 全局注册组件
Vue.component('my-count', myCount);
// Vue.component('my-count', {
//   template: `
//   <div>
//     <button @click="handleAdd">+1</button>
//     <h3>当前的值是{{count}}</h3>
//   </div>
//   `,
//   data: function () {
//     // return {
//     //   count: 0
//     // }
//     return o;
//   },
//   methods: {
//     handleAdd: function () {
//       this.count++;
//     }
//   }
// })

// Vue.filter()  Vue.component
// filters:      components


// 第一个实例
const vm = new Vue({
  el: "#app"
})


// 第二个实例
// const vm2 = new Vue({
//   el: "#app2",
//   components: {
//     // 在这个大括号中 你可以定义任意多个私有组件
//     "my-pri": {
//       template: `
//       <div class="nav">
//         <ul>
//           <li v-for="(item, index) in nameList" :key="item.id">{{item.name}}</li>
//         </ul>
//       </div>
//       `,
//       data: function () {
//         return {
//           nameList: [
//             {
//               id: 1,
//               name: "张三丰"
//             },
//             {
//               id: 2,
//               name: "张翠山"
//             },
//             {
//               id: 3,
//               name: "张奋强"
//             },
//             {
//               id: 4,
//               name: "张无忌"
//             },
//             {
//               id: 5,
//               name: "张冰雪"
//             }
//           ]
//         }
//       }
//     }
//   }
// })
// 引入组件
import myPri from "../components/myPri.vue";
const vm2 = new Vue({
  el: "#app2",
  components: {
    // 在这个大括号中 你可以定义任意多个私有组件
    "my-pri": myPri
  }
})