export var a = 10;
export var fn = function () {
  console.log("我是foo中的一个函数")
}

export var obj = {
  name: "小王吧",
  age: 19
}

export default {
  a: a,
  fn: fn,
  obj: obj
}