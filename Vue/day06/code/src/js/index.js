// 因为下面第4第6行代码是依赖于jquery 所以我要引入jquery
import $ from "jquery";
// 这是默认导入
import bar from './bar';
// 这是按需导入
// import foo, { obj } from './foo';

import { obj, fn, a } from './foo';
// 设置 每个li的背景颜色 奇数行为红色 偶数行为黄色
$('li:odd').css('backgroundColor', 'red');
// 设置 每个li的背景颜色 奇数行为红色 偶数行为黄色
$('li:even').css('backgroundColor', 'greenyellow');

/**======================================================================== */
// es6中默认的导入导出

// console.log(bar);
// bar.c();

/**======================================================================== */
// es6中的按需导入
// console.log(foo);
console.log(obj); // 如果按照默认导入的方式 是拿不到模块中的成员的
console.log(fn);
console.log(a);