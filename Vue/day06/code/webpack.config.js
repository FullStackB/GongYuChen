// 因为 12行代码需要拼接路径 所以 要引入path
const path = require('path');
// 因为 要实现 js自动注入的功能 所以要引入html-webpack-plugin
const htmlWebpackPlugin = require('html-webpack-plugin');
// 因为dist中不能有缓存文件，也不能有冗余文件 因此要使用clean-webpack-plugin
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// 1.导出配置
module.exports = {
  // 1.1 我的习惯是先来设置开发环境 还是线上环境
  mode: 'development',
  // 1.2 设置入口文件
  entry: './src/js/index.js',
  // 1.3 设置输出文件
  output: {
    // 1.3.1 设置 输出文件的路径(必须是绝对路径)
    path: path.join(__dirname, './dist/'),
    // 1.3.2 设置输出文件的名字(名字是随意的 index.js main.js app.js)
    filename: 'index.js'
  },
  // 因为要添加实时打包 实时浏览器的功能 所以要配置 webpack-dev-server 这个功能不需要引入包
  // devServer: {
  //   // 监听文件夹
  //   contentBase: './dist'
  // },
  // 接下来我要解决 问题1: html要手动引入 非常麻烦  2. dist目录中因为我改变了文件名字 功能一样名字不一样冗余文件要自动清除
  // 因为webpack并没有自带这样的功能 因此我要引入插件 插件都要放在plugins中
  plugins: [
    // 1.自动清理dist目录 
    new CleanWebpackPlugin(),
    // 2.自动注入html 
    new htmlWebpackPlugin({
      // 告诉htmlwebpackplugin  我的html模板是哪个
      template: './src/index.html',
      // 你要把文件输出到dist目录之后 模板的名字是什么
      filename: 'index.html'
    })
  ]

}