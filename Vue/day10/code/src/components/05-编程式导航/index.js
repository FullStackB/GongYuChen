import Vue from 'vue'

// 导入和安装路由
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// 导入路由组件
import app from './app.vue';
import MovieList from './MovieList.vue';
import MovieDetail from './MovieDetail.vue';

// 创建路由对象
const router = new VueRouter({
  routes: [
    { path: '/', redirect: '/movielist' },
    { path: '/movielist', component: MovieList },
    { path: '/movie/detail/:id', component: MovieDetail, props: true, name: 'MovieDetail' }
  ]
})

const vm = new Vue({
  el: '#app',
  render: c => c(app),
  router // 挂载路由对象
})
