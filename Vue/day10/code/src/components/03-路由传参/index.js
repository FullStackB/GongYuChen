import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import app from './app.vue';
import movieList from './movieList.vue';
import movieDetail from './movieDetail.vue';

const router = new VueRouter({
    routes: [
        {
        path: '/',
        redirect: '/movie/list'
    }, {
        path: '/movie/list',
        component: movieList
    }, {
        path: '/movie/detail/:id',
        component: movieDetail,
        props: true
    }
  ]
})

const vm = new Vue({
    el: '#app',
    render: h => h(app),
    router
})