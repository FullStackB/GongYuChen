// 1.引入vue
import Vue from 'vue';
// 2.引入vue-router
import VueRouter from 'vue-router';
// 3.让vue使用vue-router
Vue.use(VueRouter);
// 4.引入组件
import app from './app.vue';
import home from './home.vue';
import about from './about.vue';
import movie from './movie.vue';

import inner from './tabs/inner.vue';
import outer from './tabs/outer.vue';

// 5.配置路由规则
const router = new VueRouter({
  routes:[
    {path:'/',redirect:'/home'},
    {path:'/home',component:home},
    {path:'/about',component:about},
    {
      path:'/movie',component:movie,redirect:'/movie/inner',
      children:[
        {path:'/movie/inner',component:inner},
        {path:'/movie/outer',component:outer}
      ]
    }
  ],
  linkActiveClass:'active'
})
// 6.实例化vue 渲染主组件 挂载路由 
const vm = new Vue({
  router,
  render: h => h(app)
}).$mount('#app');