//webpack.config.js中只有一个东西是有用的 其他都是赋值
const path = require('path');
// 导出配置  nodejs中的commonjs规范
module.exports = {
  // 入口文件  意味着我打包js是从这里打包的
  // 入口文件就是程序的开始之处
  entry: './src/index.js',
  // 文件输出到哪个文件夹 名字是什么
  output: {
    filename: 'main.js',
    // path必须是绝对路径
    path: path.join(__dirname, './dist')
  }
}