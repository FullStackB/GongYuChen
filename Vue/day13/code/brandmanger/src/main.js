import Vue from "vue";
// 引入elementUI
import App from "./App.vue";
import router from "./router";
import axios from 'axios';
import ElementUI from 'element-ui';

// 配置axios
Vue.prototype.$http = axios;
axios.defaults.baseURL = "http://localhost:3333"
import 'element-ui/lib/theme-chalk/index.css';
// 使用elementUi
Vue.use(ElementUI);


Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
