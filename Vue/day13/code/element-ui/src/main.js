import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import axios from 'axios';


// 引入element中的所有的需要东西
import ElementUI from 'element-ui';
// 引入element 样式
import 'element-ui/lib/theme-chalk/index.css';
// 使用elment UI
Vue.use(ElementUI);
axios.defaults.baseURL = "http://localhost:3333"

// development  production 
// 是否配置上线产品的提示
Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
