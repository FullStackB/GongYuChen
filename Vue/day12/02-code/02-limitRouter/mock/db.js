// 1. 导入lodash
const _ = require('lodash');

// 2.导入faker
const faker = require('faker');

// 3.使用
module.exports = () => {
  const data = {
    movielist: []
  }

  data.movielist = _.times(100, n => {
    return {
      id: faker.random.uuid(),
      name: faker.random.words(),
      type: faker.random.word(),
      ctime: faker.date.recent()
    }
  })

  return data;
}