// 1.引入vue包
import Vue from 'vue'
// 2.引入路由包
import VueRouter from 'vue-router'

// 3.使用路由
Vue.use(VueRouter);


// 4.引入组件
import movieList from '../components/movieList.vue'
import movieDetail from '../components/movieDetail.vue'

// 5.配置路由规则
// const router = new VueRouter({
//   routes: [
//     {
//       path: '/',
//       redirect: '/movie/list'
//     },
//     {
//       path: '/movie/list',
//       component: movieList
//     },
//     {
//       path: '/movie/detail',
//       component: movieDetail
//     }
//   ]
// })

// export default router;



// 更简便的是这样的
export default new VueRouter({
  routes: [
    {
      path: '/',
      redirect: '/movie/list'
    },
    {
      path: '/movie/list',
      component: movieList
    },
    {
      path: '/movie/detail',
      component: movieDetail
    }
  ]
})

// export default router;