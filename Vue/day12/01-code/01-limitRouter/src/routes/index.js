// 引入vue包
import Vue from 'vue'
// 引入路由包
import VueRouter from 'vue-router'
// 使用路由
Vue.use(VueRouter);
// 引入组件
import movieList from '../components/movieList.vue'
import movieDetail from '../components/movieDetail.vue'

// 配置路由规则
export default new VueRouter({
  routes: [
    {
      path: '/',
      redirect: '/movie/list'
    },
    {
      path: '/movie/list',
      component: movieList
    },
    {
      path: '/movie/detail',
      component: movieDetail
    }
  ]
})
