// 1.引入vue包
import Vue from 'vue'
// 4.引入组件
import app from '../components/app.vue'
// 引入router
import router from '../routes'

//实例化vue 并且挂载路由
const vm = new Vue({
  el: '#app',
  render: h => h(app),
  router
})