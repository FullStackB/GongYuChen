const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// 导入解析.vue文件的的插件
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const vuePlugin = new VueLoaderPlugin()
module.exports = {
  mode: 'production',
  entry: './src/js/index.js',
  output: {
    path: path.join(__dirname, "./dist/"),
    filename: "index.js"
  },
  devServer: {
    contentBase: './dist'
  },
  module: {
    rules: [
      // 处理css
      { test: /\.css$/, use: ['style-loader', 'css-loader'] },
      // 处理vue
      { test: /\.vue$/, use: 'vue-loader' }
    ]
  },
  plugins: [
    vuePlugin,
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: 'index.html'
    })
  ]
}