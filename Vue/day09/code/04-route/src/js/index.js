// 1.引入vue
import Vue from "vue";
// 2.引入主组件
import app from "../components/App.vue";
// 3.渲染主组件
new Vue({
  render: h => h(app)
}).$mount('#app');
// 上面的代码和下面的代码 执行的效果一样 只不过上面的代码 执行的是最本质的代码 下面的代码是封装过的代码

// const vm = new Vue({
//   el: '#app',
//   render: h => h(app)
// })