// 1.引入vue
import Vue from "vue";
// 2.引入主组件
import App from "../components/App.vue";

// 3.实例化vue
new Vue({
  // 渲染主组件
  render: h => h(App)
}).$mount('#app');