-- 创建表  unsigned
create table users( 
    id int not null primary key auto_increment comment  'id',
    username varchar(25) not null comment '用户名',
    rname varchar(25) not null comment '真实姓名',
    email varchar(25) not null comment '电子邮件',
    phone varchar(25)  not null comment '电话',
    area enum('国际关系地区','北京大学','天津大学') not null comment '地区',
    admin enum ('管理员','普通用户','游客') not null comment '权限',
    state enum('正常','禁用') default '正常' not null comment '状态'
);
-- 插入数据
insert into users values
(null,'qwer','漂去微软','123@qq.com','219829721','北京大学','管理员','正常'),
(null,'asdf','阿瑟东','1146514841@qq.com','12341234','北京大学','管理员','正常'),
(null,'solider','76','76@qq.com','123265633','北京大学','管理员','正常'),
(null,'ppl','皮皮鲁','1qqq@qq.com','17737547','北京大学','管理员','正常');