const express = require('express');
const router = express.Router();
// 引入控制器模块
const controller = require('../controller');
// 查询
router.get('/usersShow',controller.usersShow);
// 添加数据
router.post('/usersAdd',controller.usersAdd);
// 回显数据
router.get('/findUser',controller.findUser);
// 更新用户信息
router.post('/updateUser',controller.updateUser);
// 删除
router.get('/usersDelete',controller.usersDelete);
// 搜索
router.post('/searchUser',controller.searchUser);
module.exports = router;