-- 题目一、
--    1、使用SHOW语句找出在服务器上当前存在什么数据库：
show databases;
--     2、创建一个数据库MYSQLDATA
create database mysqldate;
--     3、选择你所创建的数据库
use mysqldate
--     4、查看现在的数据库中存在什么表
select tables;
--     5、创建一个数据库表
create table lead(
    id int primary key auto_increment,
    lename varchar(25) ,
    homeland varchar(25),
    couple varchar(25) 
);
--     6、显示表的结构：
desc lead;
--     7、往表中插入一条数据
   insert into lead values(1001,'kon','13q','suzumiya');
   insert into lead values(null,'kon','13q','suzumiya');
--     8、更新表中一条数据
update lead set lename='1096' where id!=1001;
--     9、清空表
delete from lead
-- mysql>delete from MYTABLE;
--     10、删除表
drop table lead;

