-- 题目二、 

-- 1.创建student和score表
CREATE TABLE student (
id INT(10) NOT NULL UNIQUE PRIMARY KEY ,
name VARCHAR(20) NOT NULL ,
sex VARCHAR(4) ,
birth YEAR,
department VARCHAR(20) ,
address VARCHAR(50) 
);

-- 创建score表。SQL代码如下：
CREATE TABLE score (
id INT(10) NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT ,
stu_id INT(10) NOT NULL ,
c_name VARCHAR(20) ,
grade INT(10)
);

-- 2.为student表和score表增加记录
-- 向student表插入记录的INSERT语句如下：
INSERT INTO student VALUES( 901,'张老大', '男',1985,'计算机系', '北京市海淀区');
INSERT INTO student VALUES( 902,'张老二', '男',1986,'中文系', '北京市昌平区');
INSERT INTO student VALUES( 903,'张三', '女',1990,'中文系', '湖南省永州市');
INSERT INTO student VALUES( 904,'李四', '男',1990,'英语系', '辽宁省阜新市');
INSERT INTO student VALUES( 905,'王五', '女',1991,'英语系', '福建省厦门市');
INSERT INTO student VALUES( 906,'王六', '男',1988,'计算机系', '湖南省衡阳市');

-- 向score表插入记录的INSERT语句如下：
INSERT INTO score VALUES(NULL,901, '计算机',98);
INSERT INTO score VALUES(NULL,901, '英语', 80);
INSERT INTO score VALUES(NULL,902, '计算机',65);
INSERT INTO score VALUES(NULL,902, '中文',88);
INSERT INTO score VALUES(NULL,903, '中文',95);
INSERT INTO score VALUES(NULL,904, '计算机',70);
INSERT INTO score VALUES(NULL,904, '英语',92);
INSERT INTO score VALUES(NULL,905, '英语',94);
INSERT INTO score VALUES(NULL,906, '计算机',90);
INSERT INTO score VALUES(NULL,906, '英语',85);


-- 3.查询student表的所有记录
select * from student;

-- 4.查询student表的第2条到4条记录
 select * from student limit 1,3;

-- 5.从student表查询所有学生的学号（id）、姓名（name）和院系（department）的信息
 select id,name,department from student;

-- 6.从student表中查询计算机系和英语系的学生的信息
 select * from student where department in ('计算机系','英语系');

-- 7.从student表中查询年龄18~22岁的学生信息
 select * from student where age between 18 and 22;

-- 8.从student表中查询每个院系有多少人 
 select department,count(*) from student group by department;

-- 9.从score表中查询每个科目的最高分
 select c_name,max(grade) from score group by c_name;

-- 10.查询李四的考试科目（c_name）和考试成绩（grade）
 select c_name,grade from score where stu_id = (select id from student where name = '李四');

-- 11.用连接的方式查询所有学生的信息和考试信息
select stu_id,name,birth,department,c_name,grade,address from student,score where  score.stu_id = student.id;

-- 12.计算每个学生的总成绩
 select s.name,sum(grade) '总成绩' from student s,score c where s.id=c.stu_id group by s.name;

-- 13.计算每个考试科目的平均成绩
 select c_name,avg(grade) from score group by c_name;

-- 14.查询计算机成绩低于95的学生信息
 select * from score where c_name='计算机' and grade < 95;

-- 15.查询同时参加计算机和英语考试的学生的信息
 select s.id,s.name,s.sex,s.birth,s.address from student s,score b,score c where s.id=b.stu_id and b.c_name ='计算机' and s.id=c.stu_id and c.c_name = '英语';

-- 16.将计算机考试成绩按从高到低进行排序
 select s.name,c.grade '计算机成绩' from student s,score c where s.id=c.stu_id and c_name = '计算机' order by grade desc;

-- 17.从student表和score表中查询出学生的学号，然后合并查询结果
 select id from student union select  stu_id from score;

-- 18.查询姓张或者姓王的同学的姓名、院系和考试科目及成绩
 select name,department,c_name,grade from student,score where (name like '张%' or name like '王%')
 and student.id=score.stu_id;

-- 19.查询都是湖南的学生的姓名、年龄、院系和考试科目及成绩
select name,2019-birth '年龄',department,c_name,grade from student,score
 where  address like '湖南%' and student.id=score.stu_id;