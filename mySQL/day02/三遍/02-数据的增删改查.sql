-- 1.插入数据
-- 1.1 普通的插入数据
-- 基础语法: insert into 表名(字段列表)values(字段值列表)
insert into users(id,name,age,sex,email)values(1,'gaoyuan', 18, '1', 'gaoyuan@qq.com');
-- 1.2 不论顺序的插入数据(没必要全部写  也没必要 按照定义字段的顺序写)
insert into users(id,age,name)values(2,19,'haojian');
-- 1.3 不写字段 把所有信息插入数据
-- 基础语法: insert into 表名 values(按照字段顺序写的所有信息);
insert into users values(3,'wangduoyu', 34, '1', 'wangduoyu@qq.com');
-- 2.删除数据
-- 2.1 删除所有数据
-- 基本语法: delete from 表名  如果你是要删除表中所有数据 
delete from users;
-- 2.2 删除某一个指定条件的数据 
-- 基本语法: delete from 表名 where 条件
delete from users where age=19;
delete from users where id=2;
delete from users where name='haojian';
-- 3.更新数据
-- 3.1 更新一个数据
-- 基本语法: update 表名 set 字段名=新的值 where 条件;
update users set age=19;
update users set age= 34 where id=3;
-- 3.2 更新多个数据
-- 基本语法: update 表名 set 字段名=新的值, 字段名=新的值 where 条件;
update users set name='wagnyuan',email='wangyuan@qq.com' where id =1;
-- 4.查询数据
-- 4.1 查询所有数据
-- 基本语法: select * from 表名;
select * from users;
-- 4.2 查询某些数据
-- 基本语法: select 字段[,字段...] from 表名 
select name,age,sex from users;
-- 基本语法: select 字段[,字段...] from 表名 where 条件 
select name,age,sex from users where id=1;
