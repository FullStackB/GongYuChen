function getStyle(elem, attr) {
    return window.getComputedStyle ? window.getComputedStyle(elem, null)[attr] : elem.getCurrentStyle[attr] || 0;
}

function animate(elem, json, fn) {
    clearInterval(elem.timeId);
    elem.timeId = setInterval(function () {
        var flag = true;
        for (var attr in json) {
            var current = parseInt(getStyle(elem, attr));
            var target = json[attr];
            var step = (target - current) / 20;
            step = step > 0 ? Math.ceil(step) : Math.floor(step);
            current += step;
            elem.style[attr] = current + 'px';

            // 2.1 是否达到目标
            if (current != target) {
                flag = false;
            }
        }
        // 2.2 如果达到目标  
        if (flag) {
            clearInterval(elem.timeId);
            if (fn) {
                fn();
            }
        }
    }, 500)
}
