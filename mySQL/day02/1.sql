-- 数据库的三大组成是什么?
-- 关系数据结构
-- 关系操作集合
-- 关系完整性约束
-- 启动mysql服务
net star mysql
-- 使用操作登陆mysql系统
mysql -hlocalhost -uroot -proot
-- 使用操作退出mysql系统
quit
-- 创建students数据库 编码gbk;
create database students charset=gbk;
-- 创建study数据库 编码gbk;
create database study charset=gbk;
-- 创建stu数据库 编码为gbk;
create database stu charset=gbk;
-- 创建copy库 编码为gbk;
create database copy charset=gbk;
-- 显示全部数据库
show databases;
-- 显示已s结尾的数据库
show databases like '%s';
-- 显示全部数据库的创建语句
show create database copy;
-- 选择数据库
use stu;
-- 随意选择两个数据库修改两个数据库的编码为utf8;
alter database study charset=utf8;
alter database stu charset=utf8;
-- 删除study数据库和stu数据库;
drop database stu;
drop database study;
-- 在students创建一个普通的表 名字 infor用来存储学生信息 id 姓名 年龄 性别 qq号  电子邮箱 
create table students.infor (id int(255),name varchar(255),age int (3),sex varchar(2),qq int(11),email varchar (255));
-- 复制一个students中的表的结构 放到copy表中
create table copybiao like students.infor;
-- 显示students中所有的表
show tables ;
-- 显示students库的创建语句
show create database students;
-- 把students库中的表名infor 改为 information
rename table infor to information;
-- 删除copy库
drop database copy;
-- 在information表中插入一条数据
insert into information values(2,'aa',14,'1',112131,'1231231');
-- 在information表中同时插入5条数据
insert into information values(2,'a-a',14,'1',112131,'1231231'),(2,'awa',14,'1',112131,'1231231'),(2,'aza',14,'1',112131,'1231231'),(2,'axa',14,'1',112131,'1231231'),(2,'aca',14,'1',112131,'1231231');

-- 查询information表中的所有数据
select * from information;

-- 更新information表中的数据
update information set id=3 where name='aa';
-- 删除information中5条数据 留下1一条数据
delete from information where id=2;-- 数据库的三大组成是什么?
-- 关系数据结构
-- 关系操作集合
-- 关系完整性约束
-- 启动mysql服务
net star mysql
-- 使用操作登陆mysql系统
mysql -hlocalhost -uroot -proot
-- 使用操作退出mysql系统
quit
-- 创建students数据库 编码gbk;
create database students charset=gbk;
-- 创建study数据库 编码gbk;
create database study charset=gbk;
-- 创建stu数据库 编码为gbk;
create database stu charset=gbk;
-- 创建copy库 编码为gbk;
create database copy charset=gbk;
-- 显示全部数据库
show databases;
-- 显示已s结尾的数据库
show databases like '%s';
-- 显示全部数据库的创建语句
show create database copy;
-- 选择数据库
use stu;
-- 随意选择两个数据库修改两个数据库的编码为utf8;
alter database study charset=utf8;
alter database stu charset=utf8;
-- 删除study数据库和stu数据库;
drop database stu;
drop database study;
-- 在students创建一个普通的表 名字 infor用来存储学生信息 id 姓名 年龄 性别 qq号  电子邮箱 
create table students.infor (id int(255),name varchar(255),age int (3),sex varchar(2),qq int(11),email varchar (255));
-- 复制一个students中的表的结构 放到copy表中
create table copybiao like students.infor;
-- 显示students中所有的表
show tables ;
-- 显示students库的创建语句
show create database students;
-- 把students库中的表名infor 改为 information
rename table infor to information;
-- 删除copy库
drop database copy;
-- 在information表中插入一条数据
insert into information values(2,'aa',14,'1',112131,'1231231');
-- 在information表中同时插入5条数据
insert into information values(2,'a-a',14,'1',112131,'1231231'),(2,'awa',14,'1',112131,'1231231'),(2,'aza',14,'1',112131,'1231231'),(2,'axa',14,'1',112131,'1231231'),(2,'aca',14,'1',112131,'1231231');

-- 查询information表中的所有数据
select * from information;

-- 更新information表中的数据
update information set id=3 where name='aa';
-- 删除information中5条数据 留下1一条数据
delete from information where id=2;