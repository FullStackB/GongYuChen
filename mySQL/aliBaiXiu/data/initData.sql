-- 1.创建数据库
 create database alibx;
-- 2.使用数据库
use alibx;
-- 3.创建表
create table users(
  id int primary key auto_increment comment '用户ID',
  avatar varchar(255) default '/uploads/avatar_1.jpg',
  email varchar(25) not null comment '用户邮箱',
  slug varchar(25) not null  comment '用户别名',
  nickname varchar(25) not null comment '用户昵称',
  password varchar(20) not null comment '用户密码',
  status varchar(20) not null default 'activated' comment '未激活（unactivated）/ 激活（activated）/ 禁止（forbidden）/ 回收站（trashed）'

);
set names gbk;
-- 4.插入数据
insert into users values
(null, default, 'xiaoming@163.com', '明明','鸣人','123456','activited'),
(null, default, 'xiaohuang@163.com', '二狗子','大黄','123456','activited'),
(null, default, 'xiaohong@163.com', '小红吧','红蜘蛛','123456','activited'),
(null, default, 'xiaosun@163.com', '孙悟空','悟空','123456','activited'),
(null, default, 'xiaowang@163.com', '王大锤','大锤','123456','activited');

-- 创建文章表(文章id 文章标题 文章内容 文章别名 文章图片 文章创建时间 文章的状态  文章分类 文章作者)

create table posts(
  posts_id int primary key auto_increment comment '文章id',
  posts_title varchar(30) not null,
  posts_content text default NUll,
  posts_slug  varchar(30),
  posts_feature varchar(255),
  posts_created datetime,
  posts_status varchar(30) not null comment 'published 发布 drafted草稿 trashed 回收站',
  category_id int not null,
  user_id int not null
);