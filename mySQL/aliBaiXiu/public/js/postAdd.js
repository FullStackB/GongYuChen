// #region 图片上传
// 1.给上传图片的input添加change事件
$('#feature').on('change', function () {
  // 2.获取选择到的input中的图片信息
  let file = $(this)[0].files[0];
  // 3.实例化formdata
  let formdata = new FormData();
  // 4.把图片信息添加到formdata中
  formdata.append('feature', file);
  // 5.发起ajax请求
  $.ajax({
    type: 'post',
    url: '/postAddUpload',
    data: formdata,
    contentType: false,
    processData: false,
    success: function (data) {
      // console.log(data);
      $('.postimg').show().attr('src', data.picAddr).after('<input type="hidden" name="feature" value="' + data.picAddr + '">')
    }
  })
})
// #endregion


// #region 文章添加
// 1.设计数据库
// 2.点击保存 获取文章发表页面的所有内容
$('.btn-add').on('click', function () {
  let formdata = $('.form-add').serialize();
  // console.log(formdata);
  $.ajax({
    type: 'post',
    url: '/postAddArticle',
    data: formdata,
    success: function (data) {
      // console.log(data);
      if (data.code == '1501') {
        alert(data.message);
        location.href = '/post-add';
      }
    }
  })
})
// 3.发起ajax请求
// #endregion