// #region 功能一: 上传图片
// 1.监听input的值变化的事件 change
$('#avatar').change(function () {
  // 2.实例化formdata
  let formdata = new FormData();
  // 3.获取input中的文件
  let file = $(this)[0].files[0];
  // console.log(file);
  // 4.把文件添加到formdata中
  formdata.append('avatar', file);
  // 5.发起ajax(contentType procressdata 全部为false)
  $.ajax({
    type: 'post',
    url: '/profileUpload',
    data: formdata,
    contentType: false,
    processData: false,
    success: function (data) {
      console.log(data);

      let avatarHtml = template('profile_avatar_template', data);

      $('.pic').html(avatarHtml);

    }
  })
})

// #endregion 


// #region 功能二: 更新用户数据
$('.btn-update').on('click', function () {
  // 1.表单数据
  let formdata = $('.profile-form').serialize();
  // 2.用户id
  let userId = $(this).data('id');
  // 3.简介
  let bioText = $('#bio').val();

  // console.log(userId);
  // console.log(bioText);
  // console.log(formdata);

  $.ajax({
    type: 'post',
    url: '/profileUpdate',
    data: formdata + '&id=' + userId + '&bio=' + bioText,
    success: function (data) {
      // console.log(data);
      if (data.code == '1401') {
        alert(data.message);
        location.href = '/profile';
      }
    }
  })
})
// #endregin