$('.btn-login').on('click', function () {
  let formData = $('.login-wrap').serialize();
  $.ajax({
    type: 'post',
    url: '/login',
    data: formData,
    success: function (data) {
      // console.log(data);
      if (data.code == '1201') {
        alert(data.message);
      } else if (data.code == '1202') {
        location.href = "/index"
      }
    }
  })
})