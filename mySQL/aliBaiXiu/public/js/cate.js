// #region 显示分类列表(多条)
let cateShow = function () {
  $.ajax({
    type: 'get',
    url: '/catesFind',
    data: '',
    success: function (data) {
      // console.log(data);
      let cateshowHtml = template('cate_show_template', { list: data });
      $('tbody').html(cateshowHtml);
    }
  })
}
cateShow();
// #endregion

// #region 添加分类
$('.btn-add').on('click', function () {
  let formData = $('.add-form').serialize();
  // console.log(formData);
  $.ajax({
    type: 'post',
    url: '/cateAdd',
    data: formData,
    success: function (data) {
      // console.log(data);
      if (data.code == '1101') {
        alert(data.message);
        cateShow();
        $('.add-form')[0].reset();
      }
    }
  })
})
// #endregion

// #region 删除分类
$('tbody').on('click', '.btn-delete', function () {
  let cateId = $(this).data('id');
  // console.log(cateId);
  $.ajax({
    type: 'get',
    url: '/cateDelete',
    data: {
      id: cateId
    },
    beforeSend: function () {
      let flag = confirm('您确定要删除该分类吗？');
      if (!flag) {
        return false;
      }
    },
    success: function (data) {
      // console.log(data);
      if (data.code == '1102') {
        alert(data.message);
        cateShow();
      }
    }
  })
})
// #endregion

// #region 更新分类
$('tbody').on('click', '.btn-edit', function () {
  let cateId = $(this).data('id');
  // console.log(cateId);
  $.ajax({
    type: 'get',
    url: '/cateFind',
    data: {
      id: cateId
    },
    success: function (data) {
      // console.log(data);
      editHtml = template('cate_edit_template', data[0]);
      $('.col-md-4').html(editHtml);
    }
  })
})

$('.col-md-4').on('click', '.btn-edit', function () {
  let formData = $('.edit-form').serialize();
  let cateId = $(this).data('id');
  console.log(formData, cateId);

  $.ajax({
    type: 'post',
    url: '/cateUpdate',
    data: formData + '&id=' + cateId,
    success: function (data) {
      // console.log(data);
      if (data.code == '1103') {
        alert(data.message);
        cateShow();
        location.href = "/cate";
      }
    }
  })
})

// #endregion

// #region 批量删除
// 全选按钮选中 每一项都选中 否则都不选中
$('.checkall').on('click', function () {
  let flag = $(this).prop('checked');

  $('tbody .checkitem').prop('checked', flag);
  // if (flag) {
  //   批量删除出来
  // } else {
  //   批量删除因此
  // }
  flag ? $('.del-more').show('fast') : $('.del-more').hide();
})
//点击每一项 如果每一项都选中 全选按钮要选中
$('tbody').on('click', '.checkitem', function () {
  let checkNum = $('tbody .checkitem').length();
  let checkedNum = $('tbody .checkitem:checked').length();
  console.log(checkNum, checkedNum);
  checkNum == checkedNum ? $('.checkall').prop('checked', true) : $('.checkall').prop('checked', false);
  // 点击每一项 如果选中的项大于等于两项 那么批量删除要显示
  checkedNum >= 2 ? $('.del-more').show('fast') : $('.del-more').hide();
})
//点击批量删除按钮 开始删除分类目录
$('.del-more').on('click', function () {
  let idArr = [];
  $('.checkitem:checked').each(function (index, ele) {
    idArr.push($(ele).data('id'));
  })
  // console.log(idArr);
  $.ajax({
    type: 'get',
    url: '/catesDelete',
    data: {
      idArr: idArr
    },
    success: function (data) {
      // console.log(data);
      if (data.code == '1104') {
        alert(data.message);
        cateShow();
      }
    }
  })
})
// #endregion