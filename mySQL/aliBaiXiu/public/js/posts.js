// 请求文章列表(因为以后要多次使用这个功能 因此要写成函数)

/**
 * 
 * @param {*} pageNum  第几页也就是页码
 * @param {*} pageSize 一页显示的条数
 */
let showArticle = function (pageNum, pageSize) {
  $.ajax({
    type: 'get',
    url: '/postsArticle',
    data: {
      pageNum: pageNum || 1,
      pageSize: pageSize || 3
    },
    success: function (data) {
      console.log(data);
      let articleHtml = template('posts_article_template', { list: data.data })
      $('tbody').html(articleHtml);


      // 分页
      $('.pagination').twbsPagination({
        // 总共有多少页
        totalPages: Math.ceil(data.pageTotal / data.pageSize),
        // 显示页码的数量有几个
        visiblePages: 4,
        // 点击的时候调用方法
        onPageClick: function (event, page) {
          console.log(page)
          // $('#page-content').text('Page ' + page);
          showArticle(page, 5);
        }
      });

    }
  })
}

showArticle(1, 5);
