// 1.引入express
const express = require('express');
const multer = require('multer');
const upload = multer({});
// 2.使用express的Router来创建路由
const router = express.Router();
//3.引入 控制器
const profileCtrl = require('../controller/profileCtrl');


// 4.1 显示个人中心页面
router.get('/profile', profileCtrl.profile);

// 4.2 上传图片
router.post('/profileUpload', upload.single('avatar'), profileCtrl.profileUpload);

// 4.3 个人中心信息更新成功
router.post('/profileUpdate', profileCtrl.profileUpdate);

// 5.暴露路由
module.exports = router;