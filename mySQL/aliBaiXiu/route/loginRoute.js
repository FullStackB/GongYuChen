// 1.引入express
const express = require('express');
// 2.使用express的Router来创建路由
const router = express.Router();
//3.引入 控制器
const loginCtrl = require('../controller/loginCtrl');

// 4.1 显示页面路由
router.get('/login', loginCtrl.login);

router.post('/login', loginCtrl.userLogin)

// 4.3 退出
router.get('/logout', loginCtrl.logout);
// 5.暴露路由
module.exports = router;