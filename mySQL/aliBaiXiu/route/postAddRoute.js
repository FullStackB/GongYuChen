// 1.引入express
const express = require('express');
const multer = require('multer');
const upload = multer();
// 2.使用express的Router来创建路由
const router = express.Router();
//3.引入 控制器
const postAddCtrl = require('../controller/postAddCtrl');

// 4.1 显示文章发表页面
router.get('/post-add', postAddCtrl.postAddPage);

// 4.2 上传图片
router.post('/postAddUpload', upload.single('feature'), postAddCtrl.postAddUpload);

// 4.3 添加文章
router.post('/postAddArticle', postAddCtrl.postAddArticle);

// 5.暴露路由
module.exports = router;