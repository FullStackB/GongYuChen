// 1.引入express
const express = require('express');
// 2.使用express的Router来创建路由
const router = express.Router();
// 3.引入控制器模块
const usersCtrl = require('../controller/usersCtrl');


// 4.7 配置用户管理页面
router.get('/users', usersCtrl.Users);


// 4.7.1 全部用户查询
router.get('/usersFind', usersCtrl.usersFind);

// 4.7.2 用户添加
router.post('/userAdd', usersCtrl.userAdd);


// 4.7.3 用户删除
router.get('/userDelete', usersCtrl.userDelete);

// 4.7.4 根据id查询用户信息
router.get('/userFind', usersCtrl.userFind);

// 4.7.5 根据id更新用户信息
router.post('/userUpdate', usersCtrl.userUpdate);

// 4.7.6 批量删除多个
router.get('/usersDelete', usersCtrl.usersDelete);
module.exports = router;