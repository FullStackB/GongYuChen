// 1.引入express
const express = require('express');
// 2.使用express的Router来创建路由
const router = express.Router();
// 3.引入控制器模块
const cateCtrl = require('../controller/cateCtrl');

// 显示分类页面
router.get('/cate', cateCtrl.cateShow);

// 显示分类列表
router.get('/catesFind', cateCtrl.catesFind);

// 添加分类列表
router.post('/cateAdd', cateCtrl.cateAdd);

// 删除分类
router.get('/cateDelete', cateCtrl.cateDelete);

// 查找分类列表(一个)
router.get('/cateFind', cateCtrl.cateFind);

// 更新分类列表(一个)
router.post('/cateUpdate', cateCtrl.cateUpdate);

// 批量删除多个分类
router.get('/catesDelete', cateCtrl.catesDelete);

module.exports = router;
