// 1.引入express
const express = require('express');
// 2.使用express的Router来创建路由
const router = express.Router();
//3.引入 控制器
const postsCtrl = require('../controller/postsCtrl');

// 4.1 显示文章管理页面
router.get('/posts', postsCtrl.posts);

// 4.2 显示文字管理列表
router.get('/postsArticle', postsCtrl.postsArticle);

// 5.暴露路由
module.exports = router;