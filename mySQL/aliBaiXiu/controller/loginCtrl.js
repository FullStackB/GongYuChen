const conn = require('../data');
// #region 显示登录页面
module.exports.login = (req, res) => {
  res.render('login');
}
// #endregion


// #region 用户登录
module.exports.userLogin = (req, res) => {
  let params = [req.body.email, req.body.password];
  conn.query('select * from users where email =? and password=?', params, (error, results) => {
    if (error) return console.log(error);

    // console.log(results);
    if (results.length == 0) {
      res.json({
        code: '1201',
        message: '您的邮箱或密码错误'
      });
    } else if (results.length != 0) {
      // console.log(req.session);
      req.session.isLogin = true;
      console.log(req.session);
      res.json({
        code: '1202',
        message: '登录成功'
      })
    }
  })
}

// #endregion 
// #region 用户退出
module.exports.logout = (req, res) => {
  req.session.destroy(function (error) {
    if (error) {
      return console.log(error);
    }

    res.redirect('/login');
  })
}