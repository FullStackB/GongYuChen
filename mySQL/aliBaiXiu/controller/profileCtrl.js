const conn = require('../data');
const fs = require('fs');
// #region 显示个人信息
module.exports.profile = (req, res) => {

  if (!req.session.isLogin) {
    res.redirect('/login');
    return;
  }
  // console.log(req.session.user.id);
  conn.query('select * from users where id=?', [req.session.user.id], (error, results) => {
    if (error) return console.log(error);

    // console.log(results);
    res.render('profile', results[0]);
  })
}

// #endregion

// #region 上传头像
module.exports.profileUpload = (req, res) => {
  // console.log(req.file)
  fs.writeFile('./public/upload/' + req.file.originalname, req.file.buffer, (error) => {
    if (error) {
      return console.log(error);
    }

    res.json({
      pic: './upload/' + req.file.originalname
    })
  })

}
// #endregion


// #region 更新数据
module.exports.profileUpdate = (req, res) => {
  console.log(req.body);
  let parmas = [req.body.avatar, req.body.slug, req.body.nickname, req.body.bio, req.body.id];
  let sql = 'update users set avatar=?, slug=?, nickname=?, bio=? where id  = ?';

  conn.query(sql, parmas, (error, results) => {
    if (error) return console.log(error);

    // console.log(results);
    if (results.affectedRows) {
      res.json({
        code: '1401',
        message: '用户信息更新成功'
      })
    }
  })
}
// #endregion