const conn = require('../data');
const fs = require('fs');
// #region 显示文字发表页面
module.exports.postAddPage = (req, res) => {
  if (!req.session.isLogin) {
    res.redirect('/login');
    return;
  }

  conn.query('select * from ali_cate', (error, results) => {
    if (error) return console.log(error);

    res.render('post-add', { list: results });
  })
}

// #endregion

// #region 上传文章图片
module.exports.postAddUpload = (req, res) => {
  // console.log(req.file);
  fs.writeFile('./public/upload/' + req.file.originalname, req.file.buffer, (error) => {
    if (error) return console.log(error);

    res.json({
      picAddr: './upload/' + req.file.originalname
    })
  })
}
// #endregion

// #region 添加文章
module.exports.postAddArticle = (req, res) => {
  console.log(req.session.user.id);
  let sql = 'insert into ali_posts(posts_title, posts_content, posts_slug, posts_feature, posts_created, posts_status, category_id, user_id) values(?,?,?,?,?,?,?,?)';
  let pramas = [req.body.title, req.body.content, req.body.slug, req.body.feature, req.body.created, req.body.status, req.body.category, req.session.user.id];
  conn.query(sql, pramas, (error, results) => {
    if (error) return console.log(error);

    // console.log(results);
    if (results.affectedRows) {
      res.json({
        code: '1501',
        message: "文章添加成功"
      })
    }
  })
}
// #endregion