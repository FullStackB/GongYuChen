const conn = require('../data');

// #region 显示文字管理页面
module.exports.posts = (req, res) => {
  if (!req.session.isLogin) {
    res.redirect('/login');
    return;
  }
  res.render('posts');
}

// #endregion 


// #region 显示文字管理列表
module.exports.postsArticle = (req, res) => {
  // console.log(req.query);
  // SELECT a.posts_id, a.posts_title ,b.nickname, c.cate_name, a.posts_created, a.posts_status FROM ali_posts AS a
  // LEFT JOIN users b ON a.user_id = b.id
  // LEFT JOIN ali_cate c ON a.category_id = c.cate_id ORDER BY a.posts_created DESC LIMIT 0, 5;
  // 页码
  let pageNum = req.query.pageNum;
  // 一页显示多少条数据
  let pageSize = req.query.pageSize;

  conn.query(`SELECT COUNT(*) pageTotal FROM ali_posts;SELECT a.posts_id, a.posts_title ,b.nickname, c.cate_name, a.posts_created, a.posts_status FROM ali_posts AS a LEFT JOIN users b ON a.user_id = b.id LEFT JOIN ali_cate c ON a.category_id = c.cate_id ORDER BY a.posts_created DESC LIMIT ${(pageNum - 1) * pageSize}, ${pageSize};`, (error, results) => {
    // console.log(results);
    if (error) {
      return console.log(error);
    }

    res.json({
      code: '1601',
      message: '数据查询成功',
      pageNum: pageNum,
      pageSize: pageSize,
      pageTotal: results[0][0].pageTotal,
      data: results[1]
    })
  })

}
// #endregion