const express = require('express');
// 创建express服务器
const app = express();

const favicon = require('serve-favicon');
app.use(favicon(__dirname+'/favicon.ico'));

// 服务器的配置
// 0.配置body-parser
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
// 配置express-session
const session = require('express-session');

app.use(session({
  secret:'asfsaf',
  resave:false,
  saveUninitialized:false,
  cookie:{
    maxAge:60000
  }
}));
// 1.设置静态资源
app.use(express.static('public'));
// 2.配置路由
// 2.0 引入登录路由模块
const indexRoute = require('./route/indexRoute');
// 2.0 挂载登录路由模块
app.use(indexRoute);


// 2.1 引入用户路由模块
const usersRoute = require('./route/usersRoute');
// 2.2 挂载用户路由模块
app.use(usersRoute);

// 2.3 引入分类路由模块
const cateRoute = require('./route/cateRoute');
// 2.4 挂载分类路由模块
app.use(cateRoute);

// 2.5 引入登录路由模块
const loginRoute = require('./route/loginRoute');
// 2.6 挂载登录路由模块
app.use(loginRoute);

// 2.7引入个人中心路由模块
const profileRoute = require('./route/profileRoute');
// 2.8挂载个人中心路由模块
app.use(profileRoute);

// 2.9 文章发表页
const postAddRoute = require('./route/postAddRoute');
// 2.10 挂载文章发表页面路由
app.use(postAddRoute);

// 2.11 文章管理页面
const postsRoute = require('./route/postsRoute');
// 2.12 挂载文章管理页面路由
app.use(postsRoute);
// 3.配置模板(ejs)
// 3.0 引入 ejs包
const ejs = require('ejs');
// 3.1 设置模板引擎的后缀 ejs
app.set('view engine', 'ejs');
// 3.2 设置模板引擎所使用的模板的路径是 ./views
app.set('views', './views');




// 监听端口 并启动服务
app.listen(80, () => {
  console.log("sever is running at http://localhost/users");
})
