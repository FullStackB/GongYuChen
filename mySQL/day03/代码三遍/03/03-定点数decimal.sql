-- Decimal定点数：系统自动根据存储的数据来分配存储空间，每大概9个数就会分配四个字节来进行存储，同时小数和整数部分是分开的。
-- decimal 整数部分不能超过65, 小数部分不能超过30
-- 语法: decimal(M,D) M代表一个数最大总共有几个数字构成  D代表小数有几个数字

-- 创建数据表
create table num_deci(
  f1 float(10,2),
  d1 decimal(10,2)
);

-- 先写一个合法的数字
insert into num_deci values(12345678.90,12345678.90);

-- 写一个超出范围的数字
-- insert into num_deci values(12345678.90,12345678.999);
-- insert into num_deci values(12345678.90,123456789.999);


-- 写一个差不多可以四舍五入的
insert into num_deci values(12345678.90,99999999.99);