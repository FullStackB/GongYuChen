-- 浮点型又称之为精度类型：是一种有可能丢失精度的数据类型，数据有可能不那么准确（由其是在超出范围的时候）

-- 浮点数的取值范围 10^38次方

-- 浮点数在7位以下还是精确的 只要超过7位 那么就是没那么精确了

-- 浮点数的基本语法: float 
--   float(M,D) M代表整个数字有几位  D代表小数有几位 
-- float(6,2)  1234.23

-- 创建数据表 
create table num_float(
  f1 float,
  f2 float(10,2)
);

-- 插入数字
-- insert into num_float values(123.456,12345678.90);
-- insert into num_float values(123.456,12345678.4567);

-- -- 插入一个超过位数的数字 range超出范围了
-- insert into num_float values(123.456,123456789.00);

-- 插入一个99999999.99是系统自动设置的四舍五入造成的 我们的数据没有错, 系统可以自己承担风险 
-- insert into num_float values(123.456,99999999.99);

-- 浮点数可以插入科学计数法
-- 1*10^2 ==>1e2;  e代表底数10
-- 2*10^2 == 2e2;
-- insert into num_float values(1e2,2e2);

