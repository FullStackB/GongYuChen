-- 整数类型有5类
-- 1.tinyint 迷你整型  一个字节
-- 2.smallint 小整型  两个字节
-- 3.mediumint 中整型 三个字节
-- 4.int  标准整型 四个字节
-- 5.bigint 大整型 八个字节

-- 创建表 把数字类型写一遍
create table num_int(
  int_1 tinyint,
  int_2 smallint,
  int_3 mediumint,
  int_4 int,
  int_5 bigint
);
-- 插入数据
insert into  num_int values(1,100,1000,10000,100000,255,111);
-- 无符号 unsigned 在数据类型后添加一个属性 unsigned
-- 数字没有正负之分  只有正数

-- zerofill 填充0  
-- tinyint(2) 数字无论几位 只要是两位以内 不足两位的 添加0
-- 字段对应的值是三位 那么 就用不到zerofill
-- 只要含有zerofill 那么就必定是unsigned
alter table num_int add int_7 tinyint zerofill;

-- 改字符的类型
alter table num_int modify int_7 tinyint(2) zerofill;