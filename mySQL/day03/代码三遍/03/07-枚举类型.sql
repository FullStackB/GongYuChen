-- 枚举: 对某一类数据(性别)  世上只有三类性别: 男 女  王熙钰

-- 枚举的用途: 单选

-- 枚举的格式: enum(值,值,值);

-- 创建表
create table str_enum(
  sex enum('男','女','王熙钰')
);


-- 添加数据  男 女 男
insert into str_enum values('妖');

-- 枚举类型的存储 
-- 枚举类型存储的方式是 存的 当时设置的 值的下标

select sex +0 from str_enum;


-- 有一天 我在form表单中 添加了一个选择性别的单选框 
-- 给后台传1 2 3 因为我们已经设置好了枚举的下标
-- 只要设置下标就能通过下标设置值
insert into str_enum values(3);