-- 1.使用navcat 和 命令行方式对数据进行连接
		--连接数据库命令：
		mysql -hlocalhost -P3306 -uroot -proot
-- 2.练习数据库的 创建 字符集的设置  和  删除
		--创建数据库
		create database mydata charset=utf8;
		create database mydata2 charset=utf8;
    -- 删除数据库
    drop database mydata;

-- 3.练习数据表的创建 删除
    -- 选择数据库
    use mydata2;
    -- 创建数据表 
create table user1(name varchar(10),age int(3),sex varchar(2));
create table user2(name varchar(10),age int(3),sex varchar(2));
    -- 删除数据表
    drop table user1;
-- 4.练习对表内字段的 修改 增加  和删除
alter table user2 add  email varchar(255);
alter table user2 add id int(255) first;
alter table user2 add married varchar(2) after sex;

-- 5.新建一个 czxy 的数据库，包含两张表 user表 和 news表
create database czxy;
use czxy;
		    --  user： 包括以下字段 用户-id   用户名-username  用户性别-sex  用户邮箱-Email  用户手机号phong  用户简历-resume 
create table user (id int(255),usename  varchar(255),sex varchar(2),email varchar(255),phone varchar(11),resume varchar(255));
        --  news：包含以下字段 新闻id  新闻标题  新闻概要  新闻内容  新闻发布事件  点击数量  收藏数量
create table news (id int(255),title  varchar(255),important varchar(255),zoom varchar(255),something varchar(255),click varchar(255),bookmark varchar(255));

-- 6.分别使用navcat 和 命令行对以上数据库插入两条字段
insert into user (usename,sex) values('Jack',2);
insert into user (id,phone) values(1,10101010101);


insert into news (id,title,zoom) values(101,'The third worldWar','die or die');
insert into news (id,click) values(1,10001);
-- 7.删除表中的任意一条数据
delete from user where sex = 2;
