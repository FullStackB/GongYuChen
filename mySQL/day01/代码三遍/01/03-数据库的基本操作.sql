-- 1. 创建数据库
-- 基本语法 : create database 数据库名字;

create database users;
create database users charset=utf8;

-- 2.删除数据库
-- 基本语法: drop database 数据库名

drop database users;
-- 3.修改数据库
-- 基本语法: alter database 数据库名字 charset=字符编码集;

alter database users charset=utf8;
-- 4.显示数据库
-- 4.1 显示所有的数据库
show databases;
--4.2 显示你想显示的数据库 % 字符在百分号前面 代表以该字符开头 字符在百分号后面 代表以该字符结尾
show databases like 'g%';
show databases like '%c';
-- 4.3 如果你忘了某个字母 可以用_代替
show databases like 'use_';
-- 5.选择数据库
-- 基本语法: use 数据库名字;
use test;

