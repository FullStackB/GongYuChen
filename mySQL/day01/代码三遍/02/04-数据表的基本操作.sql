-- 1. 创建数据表
-- 1.1 普通创建表
-- 基本语法 : create table 表名(字段名 字段的类型 字段的属性,字段名 字段的类型 字段的属性...);
create table user1.profile(name varchar(10),age int(3),sex varchar(2));
-- 设置字符集: create table  表名(字段名 字段的类型 字段的属性,字段名 字段的类型 字段的属性,...) charset=字符集编码值;
create table user1.profile2(name varcahr(10),age int(3),sex varchar(2)) charset=utf8;
-- 1.2 复制已有表的结构 (只复制表的结构 不复制数据)
--  基本语法: create table 新表名 like 原表名
create table user.info like user1.profile;
-- 2.删除 
-- 2.1 删除表结构
-- 基本语法: drop table 表名[,表名]
drop table info;
drop table profile.profile2;
-- 2.2 删除表中的字段 
-- 基本语法: alter table 表名 drop 字段名
alter table infor drop id;
-- 3. 修改数据表
-- 3.1 修改数据表的表名
-- 基本语法 : rename table 旧表名 to 新表名;
rename table info  to infor;
-- 3.2 新增字段
-- 基本语法: alter table 表名 add [column] 新字段名 列类型 [列属性] [位置first/after 字段名]
alter table infor add  email varchar(255);
alter table infor add id int(255) first;
alter table infor add married varchar(2) after sex;
-- 3.3 修改字段名：
-- 基本语法: alter table 表名 change 旧字段名 新字段名 字段类型 [列属性] [新位置]
alter table infor change username username varchar(100);
-- 4.显示数据表
-- 4.1 显示所有的数据表
-- 基本语法: show tables;
show tables;
-- 4.2 显示某个数据表的创建过程 
-- 基本语法: show create table 表名;    \g   \G
show create table info;
-- 4.3 显示数据表的结构
--- 基本语法: desc 表名;
desc info;
-- 基本语法:describe 表名;
describe info;
