--  删除的基本语法: 
delete from new_worm where pinyin='a';
delete from new_worm where pinyin='g';

-- 删除复合条件的限定数量的数据
-- 基本语法: delete from 表名 where 条件 limit 数量;
delete from new_worm where pinyin="b" limit 3;
delete from new_worm where pinyin="c" limit 2;


-- Delete 删除数据的时候无法重置auto_increment
create table del_data(
    id int primary key auto_increment,
    name varchar(4) not null
);

insert into del_data(name) values 
('a'),
('o'),
('e'),
('i'),
('w'),
('u'),
('a'),
('e');

delete from del_data where name='a' limit 2;

-- 因为删除不会重置auto_increment 
-- 因此要使用Truncate 表名来重置
show create table del_data;

delete from del_data where name='b' limit 2;

-- 重置truncate 表名 比较危险 建议大家 有其主键 就不用重置
-- 销售  一天卖多少货
-- 运营 一月用户注册量 5w用户 有1万用户注销了

