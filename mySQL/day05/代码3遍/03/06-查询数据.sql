-- 完整的查询指令
-- Select select选项 字段列表 from 数据源 where条件 group by分组 having条件 order by排序 limit限制;

-- 1.创建一个含有重复数据的表
-- select 选项
-- 1.1 all 默认的
-- 1.2 Distinct 

create table query_data(
  id int primary key auto_increment,
  name varchar(5) not null
);


insert into query_data(name) values
('张三'),
('蔡明'),
('李四'),
('王五'),
('赵六'),
('蔡徐坤'),
('姚明'),
('王五'),
('张三');


-- 查询所有数据(无论是否重复)
select * from query_data;
select all * from query_data;

-- 查询非重复数据(原因就是 要所有字段都必须一致才认为是重复数据)
select distinct * from query_data;

create table query_data1(
  pinyin varchar(2)
);

insert into query_data1 values('a'),
('b'),
('c'),
('a'),
('b'),
('c');

select distinct * from query_data1;

-- 基本语法：字段名 [as] 别名

-- select distinct pinyin as name1, pinyin  as name2 from query_data1;



-- from子句
-- 1.创建 学生表
create table stu_info(
  id int primary key auto_increment,
  name varchar(6) not null,
  age tinyint not null  default 18,
  sex enum('男','女','保密') default '保密'
);

insert into stu_info(name,age,sex) values
('小白龙',default, '男'),
('小黑龙',20, '男'),
('小紫龙',21, '女'),
('小红龙',17, '男'),
('小绿龙',19, '女'),
('小粉龙',16, '男'),
('小金龙',24, '女'),
('小龙龙',30, '男'),
('小云龙',43 ,'女');
-- 2.创建 学生成绩表
create table stu_score(
  id int primary key auto_increment,
  name  varchar(6) not null,
  score tinyint not null
);
insert into stu_score(name,score) values
('小白龙',100),
('小黑龙',60),
('小紫龙',70),
('小红龙',90),
('小绿龙',99),
('小粉龙',80),
('小金龙',40),
('小龙龙',30),
('小云龙',20);



-- 起了别名的from
select name,sex as gender from stu_info;

-- 多表查询
select * from stu_info, stu_score;


-- 动态数据
-- 基本语法：from (select 字段列表 from 表) as 别名;
select * from (select name,age from stu_info ) as info;
-- 含义: 从stu_info中选区了name,age两个字段 作为一个新的表名info全部显示出来
-- 相当于select * from info了
select name from (select name,age from stu_info ) as info;