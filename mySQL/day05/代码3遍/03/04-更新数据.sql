-- 更新数据的语法: 
update table 表名 set 字段名=字段值 where 条件;

update new_worm set pinyin='g' where pintyin='a';

-- 把拼音为g的值改为a
 -- 如果没有条件, 是全表更新数据. 但是可以使用 limit 来限制更新的数量
-- update 表名 set 字段名 = 新值 [where 判断条件] limit 数量;
update new_worm set pinyin='a' where pinyin='g' limit 2;

