--having的本质和where一样，是用来进行数据条件筛选
-- 1.having是在group by子句之后: 可以针对分组数据今天统计筛选，但是where不行
-- 2.where不能使用聚合函数: 聚合函数是用在group by分组的时候， where已经运行完毕
select group_concat(name),age,count(*) as number from user where number>=2 group by sex;

select group_concat(name),age,count(*) as number from user  group by sex having number>=2;

-- 强调: having是在group by之后， group by是在where之后：where 的时候表示将数据从磁盘拿到内存，where之后的所有操作都是内存操作
