-- 蠕虫复制: 一分为二，成倍的增加。从已有的数据中获取数据，并且将获取到的数据插入到数据表中。
-- 蠕虫复制的意义: 检测数据库的效率(测试环境需要线上的数据库 那么就可以用到蠕虫复制)

-- 1.创建一个旧数据表()
create table old_worm(
    pinyin varchar(2)
);
-- 2.在旧数据表中插入数据 
insert into old_worm values ('a'),('o'),('e'),('y'),('w'),('u');
-- 3.创建一个新的数据表 去复制 旧数据表的结构
create table new_worm like old_worm;
-- 4.用到了蠕虫复制 把旧数据表的数据复制到新的数据表中
-- 基本语法: Insert into 表名 [(字段列表)] select */字段列表 from 表;
insert into new_worm select * from old_worm;
