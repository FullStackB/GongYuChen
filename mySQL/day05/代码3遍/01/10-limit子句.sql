-- 限制数量

-- 限制 找数据  只找 5条
select * from stu_info limit 5;


-- 分页: 每次取2条
-- 基本语法: limit 从第几个开始取,取几个
-- select * from stu_info limit 0,4;
-- select * from stu_info limit 4,4;
-- select * from stu_info limit 8,4;