-- 插入数据
insert into 表名(字段列表)values(字段值);
-- 插入多条数据
insert into 表名[(字段列表)]values(字段值),(字段值),(字段值),(字段值)...

-- 创建数据库
create database day05_b;
-- 使用数据库 
use day05_b;
-- 创建数据表
create table more_data(
    name varchar(5)
);
-- 插入数据
insert into more_data values('张三'),('李四'),('王五'),('小六');