-- 主键冲突: 主要发生在使用业务主键的情况下,原因是业务主键是自己定义的 不是mysql自动更新的,因此会出现主键一样的情况

-- 1.创建数据表
create table duplicate_key(
    s_id varchar(5) primary key,
    s_name varchar(5) not null
);
-- 2.插入一堆数据
insert into duplicate_key values
('s001','昭仪'),
('s002','倩儿'),
('s003','孙三'),
('s004','丽丝'),
('s005','汪芜');
-- 3.插入一个和数据库中数据的主键一样的数据 Duplicate entry 's002' for key 'PRIMARY'(拦截了键入的s002 因为是主键)
insert into duplicate_key values ('s002','熊二');
-- 4.解决主键冲突
-- 4.1 Insert into 表名 [(字段列表)] values(值列表) on duplicate key update 字段 = 新值; 相当于数据更新
insert into duplicate_key values('s002','熊二') on duplicate key update s_name='熊二';
-- 4.2 Replace into 表名 [(字段列表)] values(值列表);(相当于先删除后添加)
replace into duplicate_key values('s002','熊大');
