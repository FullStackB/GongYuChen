-- order by 排序的
-- 基本语法: order by asc 升序
-- 基本语法: order by desc 降序

select * from stu_info group by sex  order by asc;

select id,name,age from stu_info  order by age asc;

select * from stu_info order by age desc;

