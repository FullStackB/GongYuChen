-- 完整的查询指令
-- Select select选项 字段列表 from 数据源 where条件 group by分组 having条件 order by排序 limit限制;

-- 把stu_info中的龙 按照性别分 
-- 基本语法: group by 标准;
select * from stu_info group by sex;

-- 以上语句的作用就是显示第一条符合标准的结果

-- 以下的语句的含义是: 从stu_info中 通过 性别(男 女)把所有数据都分组
-- 把分组后的结果(名字) 使用group_concat()连接成一条数据
select group_concat(name),sex from stu_info group by sex;


-- 聚合函数
-- count 数量统计
select count(*) from stu_score;
-- sum() 求和
select count(*), sum(score) from stu_score;
-- max() 最大值
select max(score) from stu_score;
-- min() 最小值
select min(score) from stu_score;
-- avg() 平均数
select avg(score) from stu_score;

-- 多分组
-- 分组的依据有多个
-- 升序:自小而大 asc
-- 降序: 自大而小 desc
-- 含义: 先按照性别 分为男(1)女(2) 然后再按照年龄进行分组排序(升序)
select group_concat(name),age,sex from stu_info group by sex,age;

-- 降序
select group_concat(name),age,sex from stu_info group by sex desc,age desc;


-- 回溯统计
-- 基本语法: group by with rollup;
select count(*) from stu_info group by  age desc with rollup;

-- 多分组回溯统计
select id,count(*) from stu_info group by age desc, id desc with rollup;

