-- 创建一个数据库
create database day08_b;
-- 使用数据库
use day08_b;
-- 创建班级表
create table stu_class (
  class_id int primary key auto_increment,
  class_name varchar(20)
);
-- 创建学生表
create table stu_info (
  info_id int primary key auto_increment,
  info_name varchar(10) not null,
  info_age tinyint default 18,
  class_id int,
  foreign key(class_id) references stu_class(class_id)
);

insert into stu_class values
(null,'谋士');

insert into stu_info values
(null,'郭嘉', 32, 1);

insert into stu_info values
(null,'孔明', 28, 1);

-- 增加外键(表后增加)
-- 基本语法： Alter table 从表 add [constraint `外键名`] foreign key(外键字段) references 主表(主键);
alter table stu_info add constraint `classId` foreign key(class_id) references stu_class(class_id);
-- 查看外键 就是在查看表的创建过成
show create table stu_info;

-- 删除外键/修改外键(先删后加叫修改)
-- 外键不允许修改，只能先删除后增加
-- 基本语法：alter table 从表 drop foreign key 外键名字;
alter table stu_info drop foreign key `stu_info_ibfk_1`;

-- 基本语法：create view 视图名字 as select指令;	//可以是单表数据，也可以是连接查询，联合查询或者子查询
create view stu_info_v as select * from stu_info;
-- 使用视图
-- 基本语法：select 字段列表 from 视图名字 [子句];

select info_name from stu_info_v;
select * from stu_info_v;

-- 修改视图：本质是修改视图对应的查询语句
-- 基本语法：alter view 视图名字 as 新select指令;
alter view stu_info_v as select info_age from stu_info;

-- 查看视图
-- desc 视图名字;
desc stu_info_v;
-- show create view 视图名字
show create view stu_info_v;

-- 删除视图
-- drop view 视图的名字
drop view stu_info_v;


-- 事务
-- 显示事务
Show variables like ‘autocommit%’;
-- 关闭自动事务
set autocommit = off;
-- 开启自动服务
set autocommit = on;
 commit --提交
 rollback --回滚(清空之前操作)

--  手动事务
start transaction; --开启事务
-- 执行事务 正常添加 更新 删除
commit -- 提交操作

-- 回滚点
-- 增加
savepoint 回滚点名字;-- 字母数字下划线
rollback to 回滚点名字;-- 回滚点之后的操作清空