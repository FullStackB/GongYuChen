create table students (
    id int primary key auto_increment,
    name varchar(5) not null,
    chinese varchar(10) not null,
    english varchar(10) not null,
    math varchar (10) not null);

insert into students values
    (1,'张小明',89,78,90),(2,'李进',67,53,95),(3,'王五',87,78,77),(4,'李一',88,98,92),(5,'李来财',82,84,67),
    (6,'张进荣',55,85,45),(7,'黄蓉',75,65,30)
;
-- 1)     查询表中所有学生的信息。
select*from students ;
-- 2)     查询表中所有学生的姓名和对应的英语成绩
select name,english from students ;
-- 3)     过滤表中重复数据
distinct
select distinct english from students ;
-- 4)     统计每个学生的总分。
select name,(english+chinese+math)as '总' from students;
-- 5)     在所有学生总分数上加10分特长分。
select name,(english+chinese+math+10)as '总' from students;
-- 6)     使用别名表示学生分数。
select name,(english+chinese+math)as '总' from students;
-- 7)     查询姓名为李一的学生成绩
select name,english,chinese,math from students where name='李一';
-- 8)     查询英语成绩大于90分的同学
select name,english from students where english>90;
-- 9)     查询总分大于200分的所有同学
select name,(english+chinese+math) from students where  (english+chinese+math) >200;
-- 10)   查询英语分数在 80－90之间的同学
select name,english from students where english between 80 and 90;
-- 11)   查询数学分数为89,90,91的同学。
select name,math from students where math in(89,90,91);
-- 12)   查询所有姓李的学生英语成绩。
select name,english from students where name like '李%';
-- 13)   查询数学分80并且语文分80的同学
select name,math,chinese from students where math=80 and chinese=90;
-- 14)   查询英语80或者总分200的同学
select name,english,(chinese+english+math) from students where english=80 or (chinese+english+math)>200;
-- 15)   对数学成绩排序后输出。
select name,math from students order by math desc;
-- 16)   对总分排序后输出，然后再按从高到低的顺序输出
select name,(math+english+chinese) from students order by (math+english+chinese) desc;
-- 17）对姓李的学生成绩排序输出
select name,(math+english+chinese) from students where name like '李%' order by (math+english+chinese) desc;
-- 1）查询总成绩最高及最低的学生信息
select*from students where (english+math+chinese)=(select max(english+math+chinese) from students);
select*from students where (english+math+chinese)=(select min(english+math+chinese) from students);
-- 2）查询每一个学生的平均的成绩
select name,round((english+math+chinese)/3,2)as '平均分'from students;
-- 3）查询所有学生各科的平均成绩
select name,round(english/3,2),round(math/3,2),round(chinese/3,2)from students;
-- 4）查询数学成绩比李一同学高的学生成绩
select name from students where math>(select math from students where name ='李一');
-- 5）列出总成绩比学生“王五”高的所有学生姓名、总成绩
select name from students where( math+english+chinese)>(select ( math+english+chinese) from students where name ='王五');
-- 6）列出总成绩高于所有学生平均成绩的学生信息
select *from students where ( math+english+chinese)>(select (sum(english+chinese+math)/7)from students);