-- 1、	简述mysql数据库中char和varchar的区别	
    -- char范围是0～255，varchar最长是64k
    -- char和varchar最大的不同就是一个是固定长度，一个是可变长度
-- 2、	mysql数据库中事务的四个特性？
    -- 2.1、原子性(Atomicity)
    -- 2.2、一致性(Consistency)
    -- 2.3、隔离性(Isolation)
    -- 2.4、持久性(Durability) 
-- 3、	说出delete与truncate的区别，至少2条
    -- delete是逐条删除数据，truncate是删除表，在创建相同结构的表
    -- delete删除数据后AUTO_INCREMENT值不变，truncate删除会重置AUTO_INCREMENT值
    -- delete可以删除指定条件的数据，truncate不能
-- 4、	简述数据库管理系统、数据库、表之间是什么关系
    -- 数据库管理系统包含多个数据库，一个数据库可以包含多个表
-- 5、	写出mysql多表查询连接的种类和对应的关键字名称
    -- 内连接 inner join
    -- 左外连接 left outer join
    -- 右外连接 right outer join
-- 6、	主键和外键的作用分别是什么
    -- 主键保证的是数据的唯一性
    -- 外键保证的是数据的完整性
-- 7、	如何创建表，修改表名，删除表
    -- 创建表：CREATE TABLE 表名
    -- 修改表名：ALTER TABLE 旧表名 RENAME 新表名
    -- 删除表：DROP TABLE 表名 





-- 	店铺表
CREATE TABLE `tab_shop` (
  `shopID` varchar(10) NOT NULL COMMENT '店铺ID',
  `shopName` varchar(50) DEFAULT NULL COMMENT '店铺名称',
  PRIMARY KEY (`shopID`)
);

-- 	商品表
CREATE TABLE `tab_product` (
  `pid` varchar(32) NOT NULL COMMENT '商品ID',
  `productName` varchar(32) DEFAULT NULL COMMENT '商品名称',
  `productPrice` float DEFAULT NULL COMMENT '商品价格',
  `storeNum` int(11) DEFAULT NULL COMMENT '库存数量',
  `factory` varchar(20) DEFAULT NULL COMMENT '厂家',
  `shopID` varchar(10) DEFAULT NULL COMMENT '店铺ID',
  PRIMARY KEY (`pid`),
  KEY `FK_tab_product` (`shopID`),
  CONSTRAINT `FK_tab_product` FOREIGN KEY (`shopID`) REFERENCES `tab_shop` (`shopID`)
);

-- 	订单表
CREATE TABLE `tab_order` (
  `oid` varchar(32) NOT NULL COMMENT '订单ID',
  `shopID` varchar(30) DEFAULT NULL COMMENT '店铺ID',
  `orderStatus` varchar(4) DEFAULT NULL COMMENT '订单状态',
  `orderTime` datetime DEFAULT NULL COMMENT '订单时间',
  PRIMARY KEY (`oid`),
  KEY `FK_tab_order` (`shopID`),
  CONSTRAINT `FK_tab_order` FOREIGN KEY (`shopID`) REFERENCES `tab_shop` (`shopID`)
);

-- 	订单商品中间表
CREATE TABLE `order_item` (
  `oid` varchar(32) NOT NULL COMMENT '订单ID',
  `pid` varchar(32) NOT NULL COMMENT '商品ID',
  `num` int(11) DEFAULT NULL COMMENT '购买数量',
  `price` float DEFAULT NULL COMMENT '小计价格',
  PRIMARY KEY (`oid`,`pid`),
  KEY `FK_product` (`pid`),
  CONSTRAINT `FK_order` FOREIGN KEY (`oid`) REFERENCES `tab_order` (`oid`),
  CONSTRAINT `FK_product` FOREIGN KEY (`pid`) REFERENCES `tab_product` (`pid`)
);
-- 数据
insert  into `tab_shop`(`shopID`,`shopName`) values ('s01','天马优品'),('s02','星际数码'),('s03','乐购数码'),('s04','尚豪生活馆'),('s05','大鲜水果');

insert  into `tab_product`(`pid`,`productName`,`productPrice`,`storeNum`,`factory`,`shopID`) values ('p1','华为Mate20Plus',5000,50,'华为','s01'),('p10','note7小米',1300,30,'小米','s02'),('p2','SAMSUNG三星A70',1500,20,'三星','s01'),('p3','荣耀10华为',1300,100,'华为','s01'),('p4','HUAWEI Mate20',4000,50,'华为','s02'),('p5','三星S10',7000,60,'三星','s02'),('P6','小米9',3000,60,'小米','s03'),('p7','小米SE',2000,10,'小米','s03'),('p8','华为Mate20Plus',3999,50,'华为','s02'),('p9','华为Mate20Plus',5299,20,'华为','s03');

insert  into `tab_order`(`oid`,`shopID`,`orderStatus`,`orderTime`) values ('o1','s01','支付完成','2019-03-16 18:29:09'),('o2','s02','支付完成','2019-03-22 19:09:56'),('o3','s03','支付完成','2019-03-22 22:31:27'),('o4','s02','待支付','2019-03-22 08:10:22'),('o5','s03','待支付','2019-04-01 12:01:30'),('o6','s02','待支付','2019-04-25 19:12:26');

insert  into `order_item`(`oid`,`pid`,`num`,`price`) values ('o1','p3',2,2600),('o1','p5',1,7000),('o2','p3',1,1300),('o3','p3',3,3900),('o3','p4',1,4000),('o3','p5',1,7000),('o4','p1',2,10000),('o4','p3',1,1300),('o5','p5',1,7000),('o6','p7',3,6000);
-- 1.1. 店铺表、商品表查询
-- 1.查询商品单价大于等于4000，并且商品名称包含“Mate20”的商品
select * from tab_product where productName like "%Mate20%" and productPrice >=4000;
-- 2.查询商品单价大于等于2000并且小于等于5000的商品
select * from tab_product where  productPrice >=2000 and productPrice <=5000;

-- 3.查询商品名称是”华为Mate20Plus”的商品名称、平均价格以及最高价格
select productName,avg(productPrice),max(productPrice) from tab_product where productName ='华为Mate20Plus';
-- 4.查询商品数量大于3的店铺编号、商品数量
select shopID,count(shopID)'商品数量' from tab_product group by shopID having count(shopID) > 3;
-- 5.查询商品单价大于3000的商品名称、商品价格，并按商品单价降序排序
select productName,productPrice from  tab_product where productPrice>3000 group by productPrice desc;
-- 6.查询商品价格大于等于2000并且数量大于1的厂家名称、商品总数量，并按商品总数量降序排序。
 select factory,sum(storeNum) from  tab_product where productPrice>2000 group by storeNum desc;
-- 7.根据商品单价升序排序，并分页展示第2页数据，每页显示4条
 select * from tab_product order by productPrice limit 4,4;
-- 8.查询店铺”星际数码”的所有商品
select p.* from tab_shop s,tab_product p where s.shopID=p.shopID and s.shopName ='星际数码';

-- 9.查询所有店铺的店铺名称，该店铺中商品的最低价格、商品最高价格
 select shopName,min(p.productPrice)'最低价格',max(p.productPrice)'最高价格' from tab_shop s left join tab_product p on s.shopID=p.shopID group by shopName;

-- 10.查询”三星S10”所属店铺的商品数量、商品平均价格
 select max(s.shopName)'名称',count(productName)'商品数量',avg(p.productPrice)'平均价格' from tab_shop s,tab_product p where s.shopID=p.shopID and 
 p.shopID=(select shopID from tab_product where productName='三星S10')

-- 11.查询“华为”厂家商品价格高于“小米”厂家平均价格的商品名称、商品价格。
 select productName,productPrice from tab_product where factory='华为' and productPrice>(select avg(productPrice) from tab_product where factory='小米');

-- 12.查询”华为Mate20Plus”售价最高的店铺名称、商品价格
 select s.shopName,p.productPrice from tab_product p,tab_shop s where p.shopID=s.shopID and p.productName='华为Mate20Plus' and
  p.productPrice=(select max(productPrice) from tab_product where productName='华为Mate20Plus');

-- 1.2. 多表查询
 
-- 1.查询店铺"星际数码"所有订单中的商品ID，去除重复的商品ID并展示
 select distinct pid from  tab_shop s,tab_order o,order_item i where s.shopID=o.shopID and o.oid=i.oid and s.shopName='星际数码';

-- 2.查询店铺"星际数码"的每个待支付状态的订单总金额
select o.oid,sum(i.price)'总金额' from tab_shop s,tab_order o,order_item i where s.shopID=o.shopID and o.oid= i.oid and o.orderStatus ='待支付' and s.shopName='星际数码' group by o.oid;
-- 3.查询2019-03-22当天的交易额
	-- # 2019-03-22当天 = ("2019-03-22 00:00:00" 到 "2019-03-22 24:00:00")
	-- # 当天交易额 = 当天支付完成的总金额
 
 select sum(i.price)'交易额' from tab_order o,order_item i where o.oid=i.oid and orderStatus='支付完成' and date(orderTime)= '2019-03-22';



-- 拓展
-- 4.查询热门商品（忽略订单状态，查询哪些店铺销售了这些商品）：销量最高的商品名称、销售数量、所属店铺
 
-- select s.shopName,p.productName,t_id_num.sellNum from (select o.shopID,i.pid,sum(i.num) sellNum from tab_order o,order_item i 
-- where o.oid=i.oid and i.pid in (select t.pid from (select pid,sum(num) snum from order_item i group by pid)t 
-- where t.snum=(select sum(num) snum from order_item i group by pid order by snum desc limit 1 ) )
-- group by o.shopID,i.pid ) t_id_num, tab_product p,tab_shop s where t_id_num.shopID=s.shopID and t_id_num.pid =p.pid;
