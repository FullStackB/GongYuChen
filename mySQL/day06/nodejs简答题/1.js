// 1、什么是错误优先的回调函数？
// 如定义回调函数,第一个参数始终是错误对象,以方便检查错误


// 2. Node.js 特点（3点以上）
// 单线程，单线程的好处，减少了内存开销，操作系统的内存换页。

// 如果某一个事情，进入了，但是被I/O阻塞了，所以这个线程就阻塞了。

// 非阻塞I/O， 不会傻等I/O语句结束，而会执行后面的语句。

// 非阻塞就能解决问题了么？比如执行着小红的业务，执行过程中，小刚的I/O回调完成了，此时怎么办？？

// 事件机制，事件环，不管是新用户的请求，还是老用户的I/O完成，都将以事件方式加入事件环，等待调度。


// 3、如何确认node.js是否安装成功？

// 用命令行 输入 node -v;
// 出现版本号就是成功了


// 4、在node.js中如何导入模块 “template”？
// const tem=require('template');

// 5、如何使用npm安装第三方包，如包名为“browser-sync”

// npm i browser-sync


// 6、GET 和 POST 提交方式的区别？

// get 是已可见的形式提交

// post 提交不可见 加密


// 7、node如何绑定端口号和启动服务器

// app.listen(80,() => {
//   console.log('http:127.0.0.1:80');
// })

// 8、如何设置响应头，防止中文乱码？

// res.writeHeader(200, {
//         'Content-Type': 'text/html; charset=utf-8'
//     })


// 10、node有哪些全局对象或全局变量?（至少3点）

// 在最外层定义的变量；
// 全局对象的属性；
// 隐式定义的变量（未定义直接赋值的变量）


// 11、node有哪些定时功能?（至少3点）

// setTimeout()
// setInterval()
// setImmediate()
// process.nextTick()

// process.nextTick是在本轮循环执行的，而且是所有异步任务里面最快执行的

// setTimeout(() => console.log(1));
// setImmediate(() => console.log(2));
// process.nextTick(() => console.log(3));
// Promise.resolve().then(() => console.log(4));
// (() => console.log(5))();

// 结果:5,3,4,1,2

// node定时器的执行顺序大致为：同步任务->process.nextTick->promise->setTimeout->setImmediate


// 12、Node.js中导入模块和导入js文件写法上有什么区别？
// 导入模块
// const app=require('模块');

// 导入js文件
// const app=require('./文件名.js');或const app=require('./文件名');
