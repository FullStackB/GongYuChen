// 1、node.js如何导入fs模块
//    const fs = require('fs');
// 2、node.js中 require()和exports的区别
//    require方能看到的只有moudle.exports这个对象，他是看不到exports对象的，而我们在编写模块时用到的exports对象实际上只是对moudle.exports的引用
// 3、如何使用node.js运行文件app.js
//    require('./app.js');
// 4、如何使用使用 npm 命令安装常用的 Node.js web框架模块 express
//    npm i express
// 5、写出node.js的全局变量（2个及以上）
//    _filename console
// 6、写出node.js中异步读取和同步读取文件的方法
//    异步读取：readFileSync  同步读取：readFile
// 7、写出node.js中异步写入内容和同步写入内容到文件的方法
//    writeFileSync      writeFile