-- 内连接 inner join ,从一张表中取出所有的记录去另外一张表中匹配，利用匹配条件进行匹配，成功则保留，失败则放弃

-- 原理:
-- 1.从第一张表中取出一条记录，然后去另外一张表中进行匹配
-- 2.利用匹配条件进行匹配
-- 3.匹配到: 则保留，继续向下匹配
-- 4.匹配失败: 向下继续,如果全表匹配失败,结束

-- 语法: 表1 inner join 表2 on 匹配条件

-- 1.如果内连接没有条件(允许) 那么其实是交叉连接
-- select * from having_data inner join union_data;

-- 2.使用匹配条件进行匹配
select * from having_data inner join union_data on having_data.age=union_data.stu_age;


-- 内连接通常是在对数据有精确要求的地方使用: 必须保证两种表中都能进行数据匹配