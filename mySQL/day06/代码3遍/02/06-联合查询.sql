-- 联合查询是可合并多个相似的选择查询的结果集，等同于将一个表追加到另一个表，从而实现将两个表的查询组合到一起
-- 使用谓词 union或union all;

-- 联合查询: 将多个查询的结果合并到一起(纵向合并) 字段数不变，多个查询的记录合并；

-- 应用场景
-- 1.将同一张表中的不同的结果(需要对应多条查询语句实现)，合并到一起展示数据 男是身高升序排列 女生升高降序排列

-- 2.最常见: 在数据量大的情况下，会对表进行分表操作，需要对每张表进行部分数据统计，使用联合查询来将数据存放到一起显示、


-- 基本语法:
-- select语句1
-- union [union]选项
-- select语句2 


-- 创建一张表
create table union_data(
  stu_id varchar(6) primary key,
  stu_name varchar(5) not null,
  stu_age tinyint not null default 18,
  stu_height varchar(4) not null,
  stu_set enum('男','女','保密') default '保密'
);

-- 添加数据
insert into union_data values
('stu001', '小红',default, '156','女'),
('stu002', '小绿',21, '156','男'),
('stu003', '小黑',20, '158','女'),
('stu004', '小白',17, '168','男'),
('stu005', '小婷',16, '175','女'),
('stu006', '小雅',25, '170','男'),
('stu007', '小石头',26, '180','女'),
('stu008', '小蒋',29, '170','男'),
('stu009', '小贾',20, '174','女');


-- 显示性别为男性的数据
select * from union_data where stu_set ='男';
-- 显示性别为女性的数据
select * from union_data where stu_set="女";

-- 使用联合查询
-- 显示性别为男性的数据
(select * from union_data where stu_set ='男')
union
-- 显示性别为女性的数据
(select * from union_data where stu_set="女");

-- 使用联合查询 显示男性数据升序 显示女性数据降序
(select * from union_data where stu_set ='男' order by stu_height asc limit 100)
union
-- 显示性别为女性的数据
(select * from union_data where stu_set="女" order by stu_height desc limit 100);