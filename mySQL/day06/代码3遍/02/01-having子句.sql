-- having的本质和where一样，是用来进行数据条件筛选的

-- 1.having是再group by子句之后: 可以针对分组数据进行统计筛选，但是where不行 
-- 查询班级人数大于等于4个以上的班级
-- where 不能使用聚合函数:聚合函数是用group by分组的时候，where已经运行完毕

-- having在group by分组之后， 可以使用聚合函数或者字段别名(where 是从表中取出数据，别名是在数据进入到内存之后才有的)

-- 强调:having是在group by之后，group by是在where 之后: where 的时候表示将数据从磁盘拿到内存，where之后的所有操作都是内存操作

-- 1.创建数据库
create database day06_b;

-- 2.创建数据表
create table having_data(
  id int primary key auto_increment,
  name varchar(6) not null,
  sex enum('男','女','保密') default'保密',
  age tinyint not null default 18
);

-- 插入数据
insert into having_data(name,sex,age) values
('妲己','女',18),
('黄忠','男',50),
('张良','男',24),
('扁鹊','男',21),
('不知火舞','女',23),
('张飞','男',30),
('刘备','男',34),
('大乔','女',16),
('貂蝉','女',18);

-- 先找到having_data 的所有数据 按照 性别进行分组 显示  个数  
select count(*) from having_data group by sex;

-- 想让 数量大于=6的那一组 显示名字
select group_concat(name),count(*) from having_data group by sex;


-- select group_concat(name),count(*) from having_data group by sex;
-- where 不能使用聚合函数
-- 
--  select group_concat(name),count(*) from having_data where count(*) >=6 group by sex;  错误的

select group_concat(name),count(*) from having_data  group by sex having count(*)>=6; 