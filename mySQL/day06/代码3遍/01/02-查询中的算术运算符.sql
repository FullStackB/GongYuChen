-- +、-、*、/、%

-- 基本算术运算：通常不在条件中使用，而是用于结果运算（select 字段中）

-- 创建一张表
create table ysf_ss(
  int_1 int,
  int_2 int,
  int_3 int,
  int_4 int
);


-- 插入数据
insert into ysf_ss values
(1,2,3,4);


-- 使用算术运算符 进行结果运算
select int_1+int_2, int_2-int_1,int_2*int_3, int_3/int_2, int_3%int_2 from ysf_ss;

-- 1.mysql中除法运算结果用浮点数表示
-- 2.mysql中 如果除数为0 用null表示
-- 3.null进行任何算术运算结果都为null
