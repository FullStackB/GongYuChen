-- in运算符
-- In：在什么里面，是用来替代=，当结果不是一个值，而是一个结果集的时候
-- 基本语法： in (结果1,结果2,结果3…)，只要当前条件在结果集中出现过，那么就成立

-- 显示 id为1  3  5 6  8的用户的信息
select * from having_data where id in(1,3,5,6,8);

-- 删除name为小乔  貂蝉 不知火舞这几个人的信息
delete from having_data where name in('小乔','貂蝉','不知火舞');

-- is运算符
-- Is是专门用来判断字段是否为NULL的运算符
-- 基本语法：is null / is not null

-- 1. 找出  having_data中 age不为null的数据
select * from having_data where age is not null;

create table is_data(
  int_1 int ,
  int_2 int
);

insert into is_data values
(null, 10),
(100,200);

-- 找到 is_data中int_1为null的数据
select * from is_data where int_1 is null;

-- like运算符
-- Like运算符：是用来进行模糊匹配（匹配字符串）
-- 基本语法：like ‘匹配模式’;

-- 匹配模式中，有两种占位符：
-- _：匹配对应的单个字符
-- %：匹配多个字符


select * from having_data where name like '张%';

select * from having_data where name like '妲_';