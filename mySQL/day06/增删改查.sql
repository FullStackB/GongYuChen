-- 增
-- 插入 表名 (字段名) values (值);
insert into tag(name) values(11);
-- 在tag表中插入一条name=11 的数据
-- 改
-- 更新 表名 新的值 条件
update tag set name=22 where id=28;
-- 把tag表中id=28 的name值改为22
-- 查
-- 选择 所有的字段 from(来自) 表名
select * from tag; -- 让 tag表中的所有字段显示
-- 选择 字段名 from(来自) 表名 where 条件
select name from tag  where name=22;
--让tag表中的name值为22的name字段显示
-- 删
-- 删除 来自 表名 条件 
delete from tag where id=28;
--删除 来自tag表中id=28的数据