-- 创建数据库
create database stu;
-- 选中数据库
use stu;
--创建表
create table stu_class(
    class_id int primary key auto_increment,
    class_name varchar(18)not null 
)charset=utf8;
-- 创建表
create table stu_infor(
    infor_id int primary key auto_increment,
    infor_name varchar(18) not null,
    infor_age tinyint unsigned,
    class_id int
);
-- 插入数据
insert into stu_class values
(null, '唐'),
(null, '天'),
(null, '我'),
(null, '骷');
-- 插入数据
insert into stu_infor values
(null, '齐岳', 30, 1),
(null, '药尘', 100, 2),
(null, '林雷', 16, 3),
(null, '明人', 23, 4),
(null, '星痕', 32, 1),
(null, '雅妃', 50, 2),
(null, '李锋', 60, 3),
(null, '妄天', 44, 4),
(null, '叶音竹', 30, 1),
(null, '赵枫', 56, 2),
(null, '项羽', 24, 3),
(null, '王猛', 26, 4);


-- 标量子查询  一行一列
-- select 查询的字段 from 表名 where 条件 
select class_name from stu_class where class_id =(select class_id from stu_infor where infor_name ="
项羽");
-- 列子查询 一列多行
select class_name from stu_class where class_id in (select class_id from stu_info);
-- 行子查询 一行多列
select * from stu_info where (age) = (select max(age) from stu_info);

-- 以上三种常见子查询均属于 where子查询

-- 表子查询
--                全部字段    表名     以age为准从大到小  
select * from (select * from stu_info order by age desc) as temp group by class_id;
-- exists查询
 
select * from stu_class where exists (select * from stu_infor where stu_class.class_id = stu_infor.class_id);


--数据库备份
--整库备份
--无需登录数据库
mysqldump -h本机地址 -P端口号 -u用户名 -p密码  数据库名字 > 备份文件的地址
 
 --完整版
mysqldump -hlocalhost -P3306 -uroot -proot stu > G:\GongYuChen\mySQL\day07\onclass\on2.sql

--简单常用 
mysqldump -uroot -p123456 stu > G:\GongYuChen\mySQL\day07\onclass\on3.sql

--多表备份或单表备份
mysqldump -hlocalhost -P3306 -uroot -proot gyc admins user >  G:\GongYuChen\mySQL\day07\onclass\ongyc.sql

-- 还原数据库和表: mysql -hPup 数据库 < 文件位置
-- 需要登录 不用选择数据库
mysql -uroot -proot stu < G:\GongYuChen\mySQL\day07\onclass\on2.sql

-- source sql文件位置还原数据表
-- 选择一个数据库使用  
source  G:\GongYuChen\mySQL\day07\onclass\ongyc.sql


--创建用户
--创建 用户 用户名@本机地址 密文 by 密码;
create user 'gong'@'localhost' identified by 'root';
-- 删除密码
--删除 用户名 @ 本机地址 ;
  drop user 'gong'@'localhost';