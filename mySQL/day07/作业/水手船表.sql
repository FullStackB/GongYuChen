-- 创建sailors水手表
CREATE table sailors(
sid int(3) not null,--水手id
sname VARCHAR(8) not null,--姓名
rating int(3),-- 信誉等级
age int(3) );年龄
-- 插入数据
insert into sailors values(22,'dustin',7,45),
(29,'brustus',1,33),
(31,'lubber',8,56),
(32,'andy',11,26),
(58,'rusty',10,36),
(64,'horatio',7,35),
(71,'zorba',10,35),
(74,'horatio',9,35),
(85,'art',6,26),
(86,'john',4,17),
(95,'bob',3,64),
(96,'frodo',6,26),
(98,'tom',6,17);
-- 创建船信息表
create table boats(
    bid int(10) primary key auto_increment,--船id
    bname varchar(10) not null,--船名字
    color varchar(15) not null-- 船颜色
);
-- 插入数据
insert into boats values 
(101,"A",'red'),
(102,"B",'green'),
(103,"C",'blue'),
(104,"D",'white'),
(105,"E",'red'),
(106,"F",'blue'),
(107,"G",'green');
-- 创建rweserves水手订船信息表
create table reserves(
    sid int(10),-- 水手id
    bid int(10) not null,--船id
    rdatedate int(20) not null --日期
);
-- 插入数据
insert into reserves values 
(22,101,2010-01-08),
(22,102,2010-01-09),
(29,103,2010-01-09),
(22,104,2010-03-08),
(22,103,2010-03-10),
(32,105,2010-03-11),
(32,106,2010-03-18),
(32,102,2010-03-19),
(58,104,2010-03-20),
(64,105,2010-03-20),
(95,101,2010-04-02),
(85,102,2010-04-05),
(22,101,2010-04-07),
(22,105,2010-05-01),
(22,106,2010-06-18),
(22,107,2010-07-09),
(32,105,2010-08-06),
(29,104,2010-08-07),
(64,103,2010-09-05),
(58,102,2010-09-09),
(64,104,2010-11-03),
(64,105,2010-11-04);



-- 1.查找定了103号船的水手
select sid from reserves where bid=103;
-- 2.查找定了红色船水手的姓名
select sname from sailors where sid in(select sid from reserves where bid =101 or bid =105);
-- 3.将年龄小于30的水手级别+1
select sname,rating+1 from sailors where age <30;
-- 4.查找定了红色船而没有定绿色船的水手姓名
-*select sname from sailors where sid in(select sid from reserves where bid =101 or bid =105 and bid !=102 and bid!=107);

-- 5.查找没有定过船的水手信息
select * from sailors where sid not in(select sid from reserves );
-- 6.查找定过船而没有定过红色船的水手信息
select sname from sailors where sid  in(select sid from reserves where bid not in (select sid from reserves where bid=101 and bid=105));
select * from sailors inner join reserves on reserves.bid!=boats.bid;

-- 7.查找没有定过红色船的水手信息
select sname from sailors where sid not in(select sid from reserves where bid in(select bid from boats where color='red') );

-- 8.查找定过所有船的水手姓名和编号
select sname,sid from sailors where sid in (select sid from reserves where bid in(select bid from boats))

select sname,sid from sailors where sid in (select sid from reserves where bid=107);
-- 9.查找年龄最大的水手姓名和年龄
select sname,age from sailors where age = (select max(age) from sailors);
-- 10.统计水手表中每个级别组的平均年龄和级别组
select avg(age) from sailors where exists (select * from stu_infor where stu_class.class_id = stu_infor.class_id);


-- 11.统计水手表中每个人数不少于2人的级别组中年满18岁水手的平均年龄和级别组

-- 12.统计水手表中每个级别组的人数

-- 13.统计水手表中人数最少的级别组及人数

-- 14.查找定过船而没有定过相同的船的水手姓名

-- 15.删除名字叫lubber的水手的定船信息.

