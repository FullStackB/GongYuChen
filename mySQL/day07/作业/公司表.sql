-- # mysql数据库题目 4.21日

-- ## 一.员工部分表

-- #### 1.部门表Dept

-- * 建表语句

-- ```mysql
create table dept  (
depton float(2) primary key comment '部门号',
dname varchar(14) comment '部门名称',
loc varchar (13) comment '部门地址' ) ; 
-- ```

-- * 数据

-- ```mysql
INSERT INTO dept VALUES  (10,'ACCOUNTING','NEW YORK');  
INSERT INTO dept VALUES (20,'RESEARCH','DALLAS');  
INSERT INTO dept VALUES  (30,'SALES','CHICAGO');  
INSERT INTO dept VALUES  (40,'OPERATIONS','BOSTON');  
-- ```

-- #### 2.员工表EMP

-- * 建表语句

-- ```mysql
create table emp  
(empon float(4) primary key comment '员工编号',
ename varchar(10) comment '员工姓名',
job varchar(9) comment '员工职位',
mgr float(4)  comment '员工上级工号',
hiredate date  comment '生日',
sal float(7,2)  comment '薪水',
comm float(7,2) comment '年终奖',
depton float(2) references dept comment'部门号');  
-- ```

-- * 数据


insert into emp (empon, ename, job, mgr, hiredate, sal, comm, depton) values (7369, 'SMITH', 'CLERK', 7902, '1980-12-17', 800, null, 20);
insert into emp (empon, ename, job, mgr, hiredate, sal, comm, depton) values (7499, 'ALLEN', 'SALESMAN', 7698, '1981-02-20', 1600, 300, 30);
insert into emp (empon, ename, job, mgr, hiredate, sal, comm, depton) values (7521, 'WARD', 'SALESMAN', 7698, '1981-02-22', 1250, 500, 30);
insert into emp (empon, ename, job, mgr, hiredate, sal, comm, depton) values (7566, 'JONES', 'MANAGER', 7839, '1981-04-02', 2975, null, 20);
insert into emp (empon, ename, job, mgr, hiredate, sal, comm, depton) values (7654, 'MARTIN', 'SALESMAN', 7698, '1981-09-28', 1250, 1400, 30);
insert into emp (empon, ename, job, mgr, hiredate, sal, comm, depton) values (7698, 'BLAKE', 'MANAGER', 7839, '1981-05-01', 2850, null, 30);
insert into emp (empon, ename, job, mgr, hiredate, sal, comm, depton) values (7782, 'CLARK', 'MANAGER', 7839, '1981-06-09', 2450, null, 10);
insert into emp (empon, ename, job, mgr, hiredate, sal, comm, depton) values (7788, 'SCOTT', 'ANALYST', 7566, '1987-04-19', 3000, null, 20);
insert into emp (empon, ename, job, mgr, hiredate, sal, comm, depton) values (7839, 'KING', 'PRESIDENT', null, '1981-11-17', 5000, null, 10);
insert into emp (empon, ename, job, mgr, hiredate, sal, comm, depton) values (7844, 'TURNER', 'SALESMAN', 7698, '1981-09-08', 1500, 0, 30);
insert into emp (empon, ename, job, mgr, hiredate, sal, comm, depton) values (7876, 'ADAMS', 'CLERK', 7788, '1987-05-23', 1100, null, 20);
insert into emp (empon, ename, job, mgr, hiredate, sal, comm, depton) values (7900, 'JAMES', 'CLERK', 7698, '1981-12-03', 950, null, 30);
insert into emp (empon, ename, job, mgr, hiredate, sal, comm, depton) values (7902, 'FORD', 'ANALYST', 7566,'1981-12-02', 3000, null, 20);
insert into emp (empon, ename, job, mgr, hiredate, sal, comm, depton) values (7934, 'MILLER', 'CLERK', 7782, '1982-01-23', 1300, null, 10);


-- #### 单表基础查询

-- ##### 1) 查询没有上级的员工全部信息（mgr是空的）
select * from emp where mgr<=>'NULL';

-- ##### 2) 列出30号部门所有员工的姓名、薪资
 select ename,sal from emp where depton = (select depton from dept where depton='30');

-- ##### 3)  查询姓名包含 'A'的员工姓名、部门名称 
select emp.ename,dept.dname from emp,dept  where ename like '%A%' and emp.depton=dept.depton;

-- ##### 4) 查询员工“TURNER”的员工编号和薪资
select empon,sal from emp where ename ="TURNER";

-- ##### 5) -- 查询薪资最高的员工编号、姓名、薪资
select empon,ename,sal from emp where sal = (select max(sal) from emp);
-- ##### 6) -- 查询10号部门的平均薪资、最高薪资、最低薪资
select avg(sal),max(sal),min(sal) from emp where depton = '10';

-- #### 子查询

-- ##### 1) 列出薪金比员工“TURNER”多的所有员工姓名（ename）、员工薪资（sal）
select ename,sal from emp where sal>(select sal from emp where ename='TURNER');
-- ##### 2) 列出薪金高于公司平均薪金的所有员工姓名、薪金。
select ename,sal from emp where sal>(select avg(sal) from emp );

-- ##### 3) 列出与“SCOTT”从事相同工作的所有员工姓名、工作名称(不展示Scott的姓名、工作)
select ename,job from emp where job=(select job from emp where ename='SCOTT' )and eanme !='SCOTT';

-- ##### 4) 列出薪金高于30部门最高薪金的其他部门员工姓名、薪金、部门号。

select ename,sal,depton from emp where sal>(select max(sal) from emp where depton='30') and depton !='30';

-- ##### 5) -- 查询薪资最高的员工编号、姓名、薪资

select empon,ename,sal from emp where sal=(select max(sal) from emp );

-- ##### 6) 列出薪金高于本部门平均薪金的所有员工姓名、薪资、部门号、部门平均薪资。
select ta.* from emp ta,  
(select depton,avg(sal) avgsal from emp group by depton)tb   
where ta.depton=tb.depton and ta.sal>tb.avgsal ;      

