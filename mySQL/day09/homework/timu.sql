商品表Product  顾客表Customer  订单表 OrderItem
建表语句：
-- 商品表
create table product(
  pid  varchar(50) comment'商品id',   
  pname varchar(50) comment '商品名称',
  pirce double comment '商品价格',       
  stock  int  comment '库存量',          
   PRIMARY KEY (`pid`) 
); 
-- 顾客表
create table  customer( 
  cid  varchar(50) comment '顾客id',  
  cname varchar(50) comment'顾客姓名',
  sex  varchar(2) comment'性别 男/女',
  age  int comment'年龄 ',            
  PRIMARY KEY (`cid`)  
);
-- 订单表
create table orderItem(
cid  varchar(50) comment'顾客id',      
pid  varchar(50) comment'商品id', 
count  int comment '商品数量',           
ordertime  datetime comment '下单时间'
);

数据：
insert into product value("p001","计算机",5000,20);
insert into product value("p002","空调",2400,60);
insert into product value("p003","冰箱",1500,20);
insert into product value("p004","沙发",800,10);
insert into product value("p005","打印机",1100,50);

insert into customer value("c001","刘二","男",43);
insert into customer value("c002","张三","男",28);
insert into customer value("c003","李四","女",30);
insert into customer value("c004","王五","男",20);
insert into customer value("c005","赵六","男",50);
insert into customer value("c006","丽丽","女",50);

insert into orderitem value("c001","p001",5,"2018-4-1" );
insert into orderitem value("c001","p002",2,"2018-4-7" );
insert into orderitem value("c001","p004",2,"2018-4-7" );
insert into orderitem value("c002","p003",10,"2018-5-1" );
insert into orderitem value("c002","p005",5,"2018-5-1" );
insert into orderitem value("c003","p004",5,"2018-5-10" );
insert into orderitem value("c004","p001",5,"2018-5-10" );
insert into orderitem value("c004","p002",2,"2018-5-1" );
insert into orderitem value("c005","p003",10,"2018-5-1" );

单表基础查询
-- 1)	统计订购了商品的总人数。
select count(distinct cid) '订购人数' from orderItem;
-- 2)	统计顾客号和所订购商品总数量
select cid,sum(count) as 订单数量 from orderItem group by cid;

子查询：
-- 1)	查找没订购商品的顾客号和顾客名。
select cid,cname from customer where cid not in (select cid from orderItem );
-- 2)	查找订购商品号'p001'商品数量最多的顾客号和顾客名
select cid,cname from customer where cid in(select cid from orderitem where pid="p001"group by cid)group by cid;
-- 3)	统计至少订购2种商品的顾客id和顾客名。
select cid,cname from customer where cid in (select cid from orderitem   group by cid having count(*)>=2);

左右连接
-- 1)	查找所有顾客号和顾客名以及他们购买的商品号
selectc.cid,c.cname,o.pid  from emp e right join dept d on e.deptno = d.deptno order by deptno;

select c.cid,c.cname,o.pid from customer c right join orderItem o on c.cid=o.cid  order by c.cid;

多表查询
-- 1)	查找订购了商品"p001"的顾客号和顾客名。
select cid,cname from customer where cid in (select cid from orderitem where pid="p001" group by cid);

-- 2)	查找订购了商品号为"p001"或者"p002"的顾客号和顾客名。
select cid,cname from customer where cid in (select cid from orderitem where pid="p001" or pid="p002" group by cid);

-- 3)	查找年龄在30至40岁的顾客所购买的商品名及商品单价。
select p.pname,p.pirce from customer c,product p,orderItem o where c.cid=o.cid and o.pid=p.pid and c.age>=30 and c.age<=40;
-- 4)	查找女顾客购买的商品号，商品名和价格。
select p.pid,p.pname,p.pirce from customer c,product p,orderItem o where c.cid=o.cid and o.pid=p.pid and c.sex='女';

