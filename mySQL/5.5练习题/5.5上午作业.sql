-- 1.创建student和score表
create table student(
    id int(10) not null primary key,
    name varchar(25) not null ,
    age int(3) not null ,
    department varchar(25) not null,
    hometown varchar (25) not null 
);

create table score(
    id int(10) not null primary key auto_increment,
    sid int(10) not null,
    c_name varchar(25) not null,
    grade int(10) not null 
);
-- 2.为student表和score表增加记录

insert into student values( 901,'张伟',34,'计算机系', '北京市海淀区');
insert into student values( 902,'张威',33,'中文系', '北京市昌平区');
insert into student values( 903,'张薇薇',29,'中文系', '湖南省永州市');
insert into student values( 904,'李四', 29,'英语系', '辽宁省阜新市');
insert into student values( 905,'王舞',28,'英语系', '福建省厦门市');
insert into student values( 906,'王锍',31,'计算机系', '湖南省衡阳市');
insert into student values( 907,'王熙钰',28,'计算机系', '山西省临汾市');
insert into student values( 908,'李诗诗',20,'计算机系', '河南省开封市');

insert into score values
insert into score values(null,901, '计算机',98);
insert into score values(null,901, '英语', 80);
insert into score values(null,902, '计算机',65);
insert into score values(null,902, '中文',88);
insert into score values(null,903, '中文',95);
insert into score values(null,904, '计算机',70);
insert into score values(null,904, '英语',92);
insert into score values(null,905, '英语',94);
insert into score values(null,906, '计算机',90);
insert into score values(null,906, '英语',85);
insert into score values(null,907, '计算机',87);
insert into score values(null,907, '英语',90);
insert into score values(null,908, '英语',80);
insert into score values(null,908, '计算机',95);

-- 3.查询student表的所有记录
select * from student;
-- 4.查询student表的第2条到4条记录
select * from student limit 1,3;
-- 5.从student表查询所有学生的学号（id）、姓名（name）和院系（department）的信息
select id,name,department from student ;
-- 6.从student表中查询计算机系和英语系的学生的信息
select * from student where department in('计算机系','英语系');
-- 7.从student表中查询年龄18~35岁的学生信息
select * from student where age between 18 and 35;
-- 8.查询每个院系有多少人
select department,count(*) from student group by department;
-- 9.查询每个科目的最高分
select c_name,max(grade) from score group by c_name;
-- 10.查询李四的考试科目（c_name）和考试成绩（grade）
select c_name,grade from score where sid=(select id from student where name='李四');
-- 11.所有学生的信息和考试信息
select s.id,name,age,department,hometown,c.id,sid,c_name,grade from student s,score c where s.id=sid
-- 12.计算每个学生的总成绩
select name,sum(grade) '总成绩' from student,score where student.id=sid group by sid;
-- 13.计算每个考试科目的平均成绩
select c_name,avg(grade) from score group by c_name;
-- 14.查询计算机成绩低于95的学生信息
select *from student where id in (select sid from score where c_name='计算机' and grade<95);
-- 15.查询同时参加计算机和英语考试的学生的信息
 select s.id,s.name,s.age,s.hometown from student s,score b,score c where s.id=b.sid and b.c_name ='计算机' and s.id=c.sid and c.c_name = '英语';
-- 16.将计算机考试成绩按从高到低进行排序
select name,grade from student,score  where student.id = sid and c_name='计算机' order by grade desc;
-- 17.查询姓张或者姓王的同学的姓名、院系和考试科目及成绩
select name,department,c_name,grade from student,score where student.id=sid and name like '张%' or  student.id=sid and name like '王%';
-- 18.查询都是湖南的学生的姓名、年龄、院系和考试科目及成绩
select name,department,c_name,grade from student,score where student.id=sid and hometown like '湖南%';
