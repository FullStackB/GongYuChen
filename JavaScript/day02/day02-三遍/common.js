   // 1.创建一个自定义构造函数 --- 按钮  div元素 color
    function ChangeStyle(btnId, divId, color) {
      this.btnObj = document.getElementById(btnId);
      this.divObj = document.getElementById(divId);
      this.color = color;
    }
    // 2.在构造函数的原型上添加一个方法 用来改变颜色（最重要的）
    ChangeStyle.prototype.init = function () {
      // 2.1 给按钮添加点击事件
      // console.log(this.btnObj);
      console.log(this); // CHangeStyle
      var that = this; //缓存this
      this.btnObj.onclick = function () {
        // 2.2 找到div 并且给div设置style的值为color;
        // console.log(this) // this.btnObj
        that.divObj.style.backgroundColor = that.color;
      }

    }