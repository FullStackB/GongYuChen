import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: { 
    count: 0, 
    message: "花有重开日"
  },
  mutations: {
    handleAdd(state,steps){
      state.count +=steps
    },
    handleSub(state,steps){
      state.count= state.count-steps;
    }
  },
  actions: {
    asyncAdd(context,steps){
      setTimeout(() => {
        context.commit('handleAdd',steps)
      }, 1000);
    },
    asyncSub(context,steps){
      setTimeout(() => {
        context.commit("handleSub",steps)
      }, 1000);
    }
  },
  getters: {
    word(state){
      return "count是:"+state.count;
    }
  }
})

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  store
}).$mount('#app')
